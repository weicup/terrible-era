cc.Class({
    extends: cc.Component,
    properties: {
        node1: cc.Node,
        node2: cc.Node,
        node3: cc.Node,
        atlas: cc.SpriteAtlas,
        玩家列表: cc.Prefab,
        media_error: cc.AudioClip,
    },
    onLoad() {
        window.all = require('./全局变量');
        cc.find('code').getComponent('音效').背景音乐();
        if (cc.sys.localStorage.getItem('game_num') != null) {
            cc.find('Canvas/战绩/游玩总代数').getComponent(cc.Label).string = cc.sys.localStorage.getItem('game_num');
        } else {
            cc.find('Canvas/战绩/游玩总代数').getComponent(cc.Label).string = '0代';
        }
        if (cc.sys.localStorage.getItem('game_all') != null) {
            cc.find('Canvas/战绩/存活总月数').getComponent(cc.Label).string = cc.sys.localStorage.getItem('game_all');
        } else {
            cc.find('Canvas/战绩/存活总月数').getComponent(cc.Label).string = '0月';
        }
        if (cc.sys.localStorage.getItem('game_average') != null) {
            cc.find('Canvas/战绩/平均存活数').getComponent(cc.Label).string = cc.sys.localStorage.getItem('game_average');
        } else {
            cc.find('Canvas/战绩/平均存活数').getComponent(cc.Label).string = '0月';
        }
    },
    开关界面(target, data) {
        if (data == '开') {
            cc.find('Canvas/设置界面').active = true;
            cc.find('Canvas/一级蒙版').active = true;
            cc.director.loadScene('road');
        } else {
            cc.find('Canvas/设置界面').active = false;
            cc.find('Canvas/一级蒙版').active = false;
        }
    },
    消息提示(msg) {
        cc.audioEngine.play(this.media_error, false, all.volume);
        cc.find('Canvas/消息背景板/消息').getComponent(cc.Label).string = msg;
        cc.find('Canvas/消息背景板').getComponent(cc.Animation).play();
    },
    模式加减(target, data) {

    }

});
