// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {

    },

    日程更新() {
        cc.find('Canvas/黑屏').getComponent(cc.Animation).play('黑屏');
        setTimeout(() => {
            cc.find('Canvas/黑屏/时间').getComponent(cc.Label).string = Math.floor(self.时间 / 4) + '年' + self.时间 % 4 + '月';
            cc.find('Canvas/黑屏/时间').active = true;
            cc.find('Canvas/初始界面').active = false;
            //人物组处理
            //清空原有人物组
            cc.find('Canvas/下界面/人物池/view/content').destroyAllChildren();
            cc.find('Canvas/中左界面/view/content').destroyAllChildren();

            //生成新人物组
            for (var i = 0; i < self.人物组.length; i++) {
                var node = cc.instantiate(cc.find('game').getComponent('game').人物);
                if (self.人物组[i].性别 == '男') {
                    node.getChildByName('头像').getComponent(cc.Sprite).spriteFrame = cc.find('game').getComponent('game').男.getSpriteFrame(self.人物组[i].头像);
                } else {
                    node.getChildByName('头像').getComponent(cc.Sprite).spriteFrame = cc.find('game').getComponent('game').女.getSpriteFrame(self.人物组[i].头像);
                }
                node.getChildByName('头像').getChildByName('名字').getComponent(cc.Label).string = self.人物组[i].名字;
                node.getChildByName('工作').getComponent(cc.Label).string = self.人物组[i].工作;
                node.getChildByName('控制权').getComponent(cc.Label).string = self.人物组[i].控制权;
                node.name = i + '';
                if (self.人物组[i].控制权 == self.name) {
                    node.parent = cc.find('Canvas/下界面/人物池/view/content');
                    node.getComponent(cc.Button).clickEvents[0].customEventData = '自己';
                } else {
                    node.parent = cc.find('Canvas/中左界面/view/content');
                    node.getComponent('人物简介').注册热键();
                }
                cc.find('Canvas/下界面/人物状态/信息/view/content').active = false;
            }
            cc.find('Canvas/黑屏/时间').active = false;
            cc.find('Canvas/黑屏').getComponent(cc.Animation).play('白屏');
            cc.find('Canvas/日程详情/工作/view/content').destroyAllChildren();

            // 日程详情添加人物工作
            setTimeout(() => {
                for (var i = 0; i < self.人物组.length; i++) {
                    var node = cc.instantiate(cc.find('game').getComponent('game').日程详情_工作);
                    if (self.人物组[i].工作 != '休息') {
                        //复制节点进日程详情
                        if (self.人物组[i].性别 == '男') {
                            node.getChildByName('头像').getComponent(cc.Sprite).spriteFrame = cc.find('game').getComponent('game').男.getSpriteFrame(self.人物组[i].头像);
                        } else {
                            node.getChildByName('头像').getComponent(cc.Sprite).spriteFrame = cc.find('game').getComponent('game').女.getSpriteFrame(self.人物组[i].头像);
                        }
                        node.getChildByName('名字').getComponent(cc.Label).string = self.人物组[i].名字 + '(' + self.人物组[i].控制权 + ')';
                        node.getChildByName('工作').getComponent(cc.Label).string = self.人物组[i].工作;
                        node.getChildByName('收获量').getComponent(cc.Label).string = self.人物组[i].收获量;

                        //食物采集计算
                        if (self.人物组[i].工作 == '采集' || self.人物组[i].工作 == '狩猎') {
                            self.food[0] = self.food[0] * 1 + self.人物组[i].收获量 * 1;
                            cc.find('Canvas/上界面/区域界面/资源/食物/2').getComponent(cc.Label).string = self.food[0] + '/' + self.food[1];
                        }
                        //木头采集计算
                        else if (self.人物组[i].工作 == '砍伐') {
                            self.mood[0] = self.mood[0] * 1 + self.人物组[i].收获量 * 1;
                            if (self.mood[0] >= self.mood[1]) {
                                self.mood[0] = self.mood[1];
                            }
                            cc.find('Canvas/上界面/区域界面/资源/木头/2').getComponent(cc.Label).string = self.mood[0] + '/' + self.mood[1];
                        }
                        //石头采集计算
                        else if (self.人物组[i].工作 == '挖掘') {
                            self.stone[0] = self.stone[0] * 1 + self.人物组[i].收获量 * 1;
                            if (self.stone[0] >= self.stone[1]) {
                                self.stone[0] = self.stone[1];
                            }
                            cc.find('Canvas/上界面/区域界面/资源/石头/2').getComponent(cc.Label).string = self.stone[0] + '/' + self.stone[1];
                        }
                        //经验值计算
                        node.getChildByName('经验值').getComponent(cc.Label).string = self.人物组[i].收获经验值;
                        //绑定父节点
                        node.parent = cc.find('Canvas/日程详情/工作/view/content');
                    }
                }
                //食物消耗计算
                self.food[0] = (self.food[0] * 1 - msg.进食量 * 1).toFixed(2);
                if (self.food[0] <= 0) {
                    console.log('游戏结束');
                }
                if (self.food[0] >= self.food[1]) {
                    self.food[0] = Math.floor(self.food[1]);
                }

                //食物更新
                cc.find('Canvas/上界面/区域界面/资源/食物/2').getComponent(cc.Label).string = self.food[0] + '/' + self.food[1];

                //打开日程详情界面
                cc.find('Canvas').pauseSystemEvents();
                cc.find('Canvas/二级遮罩').active = true;
                cc.find('Canvas/日程详情').active = true;

                //准备状态重置
                cc.find('Canvas/中右界面/准备').getComponent(cc.Button).interactable = true;
                cc.find('Canvas/中右界面/准备').getComponent(cc.Button).clickEvents[1].customEventData = '准备';
                cc.find('Canvas/中右界面/准备').getComponent(cc.Sprite).spriteFrame = cc.find('game').getComponent('game').准备状态.getSpriteFrame('准备');

                cc.find('Canvas/下界面/人物池/view/选中框').active = false;
                self.选中 = -1;
                cc.find('Canvas/日程详情').getComponent(cc.Animation).play();
                cc.find('Canvas').resumeSystemEvents();
            }, 3000);
        }, 3000);
    }
});
