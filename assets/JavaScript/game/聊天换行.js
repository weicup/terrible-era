// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        node1: cc.Node,
    },
    onLoad() {
        var len = this.node.getComponent(cc.Label).string.length;
        if (len <= 13) {
            this.node.width = len * 42;
        } else {
            this.node.width = 528;
        }
    },



});
