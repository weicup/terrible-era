// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        game_人物列表: cc.Prefab,
        media_error: cc.AudioClip,
    },
    属性介绍(data) {
        switch (data) {
            //属性
            case '力量':
                return ('力量：影响砍伐、挖掘');
                break;
            case '智力':
                return ('智力：影响祈福、研究');
                break;
            case '技巧':
                return ('技巧：影响采集、建造、狩猎');
                break;
            case '运气':
                return ('运气：影响额外获得物品几率');
                break;

            //优点: ['小鸟胃', '后起之秀', '好学', '完美', '幸运儿', '强壮']
            case '小鸟胃':
                return ('小鸟胃：每日进食量小');
                break;
            case '后起之秀':
                return ('后起之秀：每年随机获得属性点');
                break;
            case '好学':
                return ('好学：经验获取速度快');
                break;
            case '完美':
                return ('完美：没有缺点');
                break;
            case '幸运儿':
                return ('幸运儿：额外增加获得物品几率');
                break;
            case '强壮':
                return ('强壮：不会生病');
                break;
            case '未知':
                return ('人物缺点将在第二年开始暴露');
                break;
        }
    },
    消息提示(msg) {
        cc.find('Canvas/消息背景板').active = true;
        cc.audioEngine.play(this.media_error, false, all.volume);
        cc.find('Canvas/消息背景板').getComponent(cc.Animation).play();
        cc.find('Canvas/消息背景板/消息').getComponent(cc.Label).string = msg;

    },
    选择人物详情() {
        cc.find('Canvas/选择界面/详情/id').getComponent(cc.Label).string = this.node.name;
        cc.find('Canvas/选择界面/详情/名字').getComponent(cc.Label).string = all.prerole[this.node.name].名字;
        cc.find('Canvas/选择界面/详情/优点').getComponent(cc.Label).string = all.prerole[this.node.name].优点;
        cc.find('Canvas/选择界面/详情附属/优点').getComponent(cc.Label).string = this.属性介绍(all.prerole[this.node.name].优点);
        cc.find('Canvas/选择界面/详情/力量').getComponent(cc.Label).string = all.prerole[this.node.name].力量;
        cc.find('Canvas/选择界面/详情/智力').getComponent(cc.Label).string = all.prerole[this.node.name].智力;
        cc.find('Canvas/选择界面/详情/技巧').getComponent(cc.Label).string = all.prerole[this.node.name].技巧;
        cc.find('Canvas/选择界面/详情/运气').getComponent(cc.Label).string = all.prerole[this.node.name].运气;
        if (all.prerole[this.node.name].性别 == 1) {
            cc.find('Canvas/选择界面/详情/头像').getComponent(cc.Sprite).spriteFrame = cc.find('code').getComponent('game_初始化').game_男人物.getSpriteFrame(all.prerole[this.node.name + ''].头像);
        } else {
            cc.find('Canvas/选择界面/详情/头像').getComponent(cc.Sprite).spriteFrame = cc.find('code').getComponent('game_初始化').game_女人物.getSpriteFrame(all.prerole[this.node.name + ''].头像);
        }
        cc.find('Canvas/选择界面/选中框').x = this.node.x;
        cc.find('Canvas/选择界面/选中框').y = this.node.y - 29.6;
        cc.find('Canvas/选择界面/选中框').active = true;
        cc.find('Canvas/选择界面/详情').active = true;
        cc.find('Canvas/选择界面/详情附属').active = true;
        cc.find('Canvas/选择界面').getComponent(cc.Animation).play();
    },
    选择人物() {
        var num = cc.find('Canvas/选择界面/详情/id').getComponent(cc.Label).string;
        if (num == '-1') {
            this.消息提示('请选择人物');
            return;
        }
        all.role.push(all.prerole[num]);
        all.selectNum++;
        if (all.selectNum == 4) {
            var male = false;
            for (var i = 0; i < all.role.length; i++) {
                if (all.role[i].性别 == 1) {
                    male = true;
                }
            }
            if (male != true) {
                this.消息提示('请选择至少一名男性');
                all.role.pop();
                all.selectNum--;
                return;
            }
            cc.find('Canvas/选择界面').destroy();
            //面板人物初始化
            for (var i = 0; i < all.role.length; i++) {
                var node = cc.instantiate(this.game_人物列表);
                if (all.role[i].性别 == 1) {
                    node.getChildByName('头像').getComponent(cc.Sprite).spriteFrame = cc.find('code').getComponent('game_初始化').game_男人物.getSpriteFrame(all.role[i].头像);
                } else {
                    node.getChildByName('头像').getComponent(cc.Sprite).spriteFrame = cc.find('code').getComponent('game_初始化').game_女人物.getSpriteFrame(all.role[i].头像);
                }
                node.getChildByName('状态').getComponent(cc.Label).string = all.role[i].状态;
                node.getChildByName('工作').getComponent(cc.Label).string = all.role[i].工作;
                node.getChildByName('名字').getComponent(cc.Label).string = all.role[i].名字;
                node.name = i + '';
                node.parent = cc.find('Canvas/人物界面/人物列表/view/content');
            }
            //工作面板初始化
            for (var i = 0; i < all.role.length; i++) {
                var node = cc.instantiate(cc.find('code').getComponent('game_初始化').game_工作面板人物);
                node.name = i + '';
                if (all.role[i].性别 == 1) {
                    node.getChildByName('头像').getComponent(cc.Sprite).spriteFrame = cc.find('code').getComponent('game_初始化').game_男人物.getSpriteFrame(all.role[i].头像);
                } else {
                    node.getChildByName('头像').getComponent(cc.Sprite).spriteFrame = cc.find('code').getComponent('game_初始化').game_女人物.getSpriteFrame(all.role[i].头像);
                }
                node.getChildByName('名字').getComponent(cc.Label).string = all.role[i].名字;
                node.parent = cc.find('Canvas/工作面板/区域/view/content');
            }
            //正式开始
            cc.find('code').getComponent('game_初始化').日程过渡();
        }
        cc.find('Canvas/选择界面/详情/id').getComponent(cc.Label).string = '-1';
        cc.find('Canvas/选择界面/详情').active = false;
        cc.find('Canvas/选择界面/详情附属').active = false;
        cc.find('Canvas/选择界面/选中框').active = false;
        cc.find('Canvas/选择界面/选择人物').getChildByName(num).destroy();
    },
    人物详情() {
        cc.find('Canvas/人物界面/人物列表/view/选中框').active = true;
        cc.find('Canvas/人物界面/人物列表/view/选中框').y = this.node.y + 235;
        cc.find('Canvas/详情界面/详情').active = true;
        var num = this.node.name * 1;
        if (all.role[num].性别 == '1') {
            cc.find('Canvas/详情界面/详情/view/content/头像').getComponent(cc.Sprite).spriteFrame = cc.find('code').getComponent('game_初始化').game_男人物.getSpriteFrame(all.role[num].头像);
            cc.find('Canvas/详情界面/详情/view/content/性别').getComponent(cc.Label).string = '男';
        } else {
            cc.find('Canvas/详情界面/详情/view/content/头像').getComponent(cc.Sprite).spriteFrame = cc.find('code').getComponent('game_初始化').game_女人物.getSpriteFrame(all.role[num].头像);
            cc.find('Canvas/详情界面/详情/view/content/性别').getComponent(cc.Label).string = '女';
        }
        var array = ['名字', '状态', '工作', '优点', '缺点', '年龄', '力量', '智力', '技巧', '运气'];
        for (var i = 0; i < array.length; i++) {
            cc.find('Canvas/详情界面/详情/view/content/' + array[i]).getComponent(cc.Label).string = all.role[num][array[i]];
        }

        //技能
        var array = Object.keys(all.role[num].技能);
        for (var i = 0; i < array.length; i++) {
            cc.find('Canvas/详情界面/详情/view/content/' + array[i]).getComponent(cc.ProgressBar).progress = (all.role[num].技能[array[i]][0] / (all.role[num].技能[array[i]][1] * 10)).toFixed(2);
            cc.find('Canvas/详情界面/详情/view/content/' + array[i]).getChildByName('等级').getComponent(cc.Label).string = all.role[num].技能[array[i]][1] + '级';
            console.log(all.role);
            console.log(all.role[num].技能[array[i]][0]);
            cc.find('Canvas/详情界面/详情/view/content/' + array[i]).getChildByName('经验值').getComponent(cc.Label).string = (all.role[num].技能[array[i]][0]).toFixed(2) + '/' + all.role[num].技能[array[i]][1] * 10;
        }
    },
    点击属性介绍(target, data) {

    },
    关闭面板(target, data) {
        cc.find('Canvas').getChildByName(data).active = false;
        cc.find('Canvas/一级遮罩').active = false;
    },
    打开面板(target, data) {
        cc.find('Canvas/' + data).active = true;
        cc.find('Canvas/一级遮罩').active = true;
    },
    更换工作(target, data) {
        var num = this.node.name;
        if (data == all.role[num].工作) {
            return;
        }
        switch (data) {
            case '空闲':
                cc.find('Canvas/人物界面/人物列表/view/content/' + num + '/工作').color = cc.color(0, 0, 0, 255);
                break;
            case '采集':
                cc.find('Canvas/人物界面/人物列表/view/content/' + num + '/工作').color = cc.color(0, 0, 255, 255);
                break;
            case '狩猎':
                cc.find('Canvas/人物界面/人物列表/view/content/' + num + '/工作').color = cc.color(0, 155, 155, 255);
                break;
        }
        this.node.getChildByName(data).color = cc.color(255, 255, 255, 255);
        this.node.getChildByName(all.role[num].工作).color = cc.color(0, 0, 0, 255);
        cc.find('Canvas/人物界面/人物列表/view/content/' + num + '/工作').getComponent(cc.Label).string = data;
        all.role[num].工作 = data;
    },
    测试中() {
        this.消息提示('暂未开放');
    }
});
