"use strict";
cc._RF.push(module, 'db6eeHvl69KM7LGWJ/eg5Fd', '人物简介');
// JavaScript/game/人物简介.js

"use strict";

// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
cc.Class({
  "extends": cc.Component,
  properties: {},
  注册热键: function _() {
    this.node.on(cc.Node.EventType.TOUCH_START, this.touchStart, this);
    this.node.on(cc.Node.EventType.TOUCH_END, this.touchEnd, this);
    this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.touchEnd, this);
  },
  人物简介_自己: function _(target, data) {
    if (data == '自己') {
      //选中框
      cc.find('Canvas/下界面/人物池/view/选中框').active = true;
      cc.find('Canvas/下界面/人物池/view/选中框').x = -4.5;
      cc.find('Canvas/下界面/人物池/view/选中框').y = this.node.y + 246; //信息

      var node = cc.find('Canvas/下界面/人物状态/信息/view/content');
      node.active = true;
      num = this.node.name;
      self.选中 = num;

      if (self.人物组[num].性别 == '男') {
        node.getChildByName('头像').getComponent(cc.Sprite).spriteFrame = cc.find('game').getComponent('game').男.getSpriteFrame(self.人物组[num].头像);
      } else {
        node.getChildByName('头像').getComponent(cc.Sprite).spriteFrame = cc.find('game').getComponent('game').女.getSpriteFrame(self.人物组[num].头像);
      }

      cc.find('Canvas/下界面/人物状态/信息/view/content/1/介绍/介绍/人物序号').getComponent(cc.Label).string = self.人物组[num].序号;
      cc.find('Canvas/下界面/人物状态/信息/view/content/1/介绍/介绍/人物名字').getComponent(cc.Label).string = self.人物组[num].名字;
      cc.find('Canvas/下界面/人物状态/信息/view/content/1/介绍/介绍/人物性别').getComponent(cc.Label).string = self.人物组[num].性别;
      cc.find('Canvas/下界面/人物状态/信息/view/content/1/介绍/介绍/人物年龄').getComponent(cc.Label).string = self.人物组[num].年龄 + '月';
      cc.find('Canvas/下界面/人物状态/信息/view/content/1/介绍/介绍/人物状态').getComponent(cc.Label).string = self.人物组[num].状态;
      cc.find('Canvas/下界面/人物状态/信息/view/content/1/介绍/介绍/人物优点').getComponent(cc.Label).string = self.人物组[num].优点;
      cc.find('Canvas/下界面/人物状态/信息/view/content/1/介绍/介绍/人物优点').getComponent(cc.Button).clickEvents[1].customEventData = self.人物组[num].优点;

      if (self.时间 >= 5) {
        cc.find('Canvas/下界面/人物状态/信息/view/content/1/介绍/介绍/人物缺点').getComponent(cc.Label).string = self.人物组[num].缺点;
      } //属性


      cc.find('Canvas/下界面/人物状态/信息/view/content/1/属性/属性/力量').getComponent(cc.Label).string = self.人物组[num].力量;
      cc.find('Canvas/下界面/人物状态/信息/view/content/1/属性/属性/智力').getComponent(cc.Label).string = self.人物组[num].智力;
      cc.find('Canvas/下界面/人物状态/信息/view/content/1/属性/属性/耐力').getComponent(cc.Label).string = self.人物组[num].耐力;
      cc.find('Canvas/下界面/人物状态/信息/view/content/1/属性/属性/专注').getComponent(cc.Label).string = self.人物组[num].专注;
      cc.find('Canvas/下界面/人物状态/信息/view/content/1/属性/属性/幸运').getComponent(cc.Label).string = self.人物组[num].幸运;
      cc.find('Canvas/下界面/人物状态/信息/view/content/1/属性/属性/气质').getComponent(cc.Label).string = self.人物组[num].气质; //技能

      cc.find('Canvas/下界面/人物状态/信息/view/content/技能/技能/采集/等级').getComponent(cc.Label).string = self.人物组[num].采集等级 + '级';
      cc.find('Canvas/下界面/人物状态/信息/view/content/技能/技能/狩猎/等级').getComponent(cc.Label).string = self.人物组[num].狩猎等级 + '级';
      cc.find('Canvas/下界面/人物状态/信息/view/content/技能/技能/砍伐/等级').getComponent(cc.Label).string = self.人物组[num].砍伐等级 + '级';
      cc.find('Canvas/下界面/人物状态/信息/view/content/技能/技能/挖掘/等级').getComponent(cc.Label).string = self.人物组[num].挖掘等级 + '级';
      cc.find('Canvas/下界面/人物状态/信息/view/content/技能/技能/研究/等级').getComponent(cc.Label).string = self.人物组[num].研究等级 + '级';
      cc.find('Canvas/下界面/人物状态/信息/view/content/技能/技能/祈福/等级').getComponent(cc.Label).string = self.人物组[num].祈福等级 + '级';
      cc.find('Canvas/下界面/人物状态/信息/view/content/技能/技能/制造/等级').getComponent(cc.Label).string = self.人物组[num].制造等级 + '级';
      cc.find('Canvas/下界面/人物状态/信息/view/content/技能/技能/建造/等级').getComponent(cc.Label).string = self.人物组[num].建造等级 + '级';
      cc.find('Canvas/下界面/人物状态/信息/view/content/技能/技能/采集/进度条背景/经验').getComponent(cc.Label).string = self.人物组[num].采集经验.toFixed(2) + '/' + self.人物组[num].采集等级 * 10;
      cc.find('Canvas/下界面/人物状态/信息/view/content/技能/技能/狩猎/进度条背景/经验').getComponent(cc.Label).string = self.人物组[num].狩猎经验.toFixed(2) + '/' + self.人物组[num].狩猎等级 * 10;
      cc.find('Canvas/下界面/人物状态/信息/view/content/技能/技能/砍伐/进度条背景/经验').getComponent(cc.Label).string = self.人物组[num].砍伐经验.toFixed(2) + '/' + self.人物组[num].砍伐等级 * 10;
      cc.find('Canvas/下界面/人物状态/信息/view/content/技能/技能/挖掘/进度条背景/经验').getComponent(cc.Label).string = self.人物组[num].挖掘经验.toFixed(2) + '/' + self.人物组[num].挖掘等级 * 10;
      cc.find('Canvas/下界面/人物状态/信息/view/content/技能/技能/研究/进度条背景/经验').getComponent(cc.Label).string = self.人物组[num].研究经验.toFixed(2) + '/' + self.人物组[num].研究等级 * 10;
      cc.find('Canvas/下界面/人物状态/信息/view/content/技能/技能/祈福/进度条背景/经验').getComponent(cc.Label).string = self.人物组[num].祈福经验.toFixed(2) + '/' + self.人物组[num].祈福等级 * 10;
      cc.find('Canvas/下界面/人物状态/信息/view/content/技能/技能/制造/进度条背景/经验').getComponent(cc.Label).string = self.人物组[num].制造经验.toFixed(2) + '/' + self.人物组[num].制造等级 * 10;
      cc.find('Canvas/下界面/人物状态/信息/view/content/技能/技能/建造/进度条背景/经验').getComponent(cc.Label).string = self.人物组[num].建造经验.toFixed(2) + '/' + self.人物组[num].建造等级 * 10;
      cc.find('Canvas/下界面/人物状态/信息/view/content/技能/技能/采集/进度条背景/进度条2').width = self.人物组[num].采集经验 / (self.人物组[num].采集等级 * 10) * 205;
      cc.find('Canvas/下界面/人物状态/信息/view/content/技能/技能/狩猎/进度条背景/进度条2').width = self.人物组[num].狩猎经验 / (self.人物组[num].狩猎等级 * 10) * 205;
      cc.find('Canvas/下界面/人物状态/信息/view/content/技能/技能/砍伐/进度条背景/进度条2').width = self.人物组[num].砍伐经验 / (self.人物组[num].砍伐等级 * 10) * 205;
      cc.find('Canvas/下界面/人物状态/信息/view/content/技能/技能/挖掘/进度条背景/进度条2').width = self.人物组[num].挖掘经验 / (self.人物组[num].挖掘等级 * 10) * 205;
      cc.find('Canvas/下界面/人物状态/信息/view/content/技能/技能/研究/进度条背景/进度条2').width = self.人物组[num].研究经验 / (self.人物组[num].研究等级 * 10) * 205;
      cc.find('Canvas/下界面/人物状态/信息/view/content/技能/技能/祈福/进度条背景/进度条2').width = self.人物组[num].祈福经验 / (self.人物组[num].祈福等级 * 10) * 205;
      cc.find('Canvas/下界面/人物状态/信息/view/content/技能/技能/制造/进度条背景/进度条2').width = self.人物组[num].制造经验 / (self.人物组[num].制造等级 * 10) * 205;
      cc.find('Canvas/下界面/人物状态/信息/view/content/技能/技能/建造/进度条背景/进度条2').width = self.人物组[num].建造经验 / (self.人物组[num].建造等级 * 10) * 205;
    }
  },
  touchStart: function touchStart() {
    //信息
    var num = this.node.name;
    var node = cc.find('Canvas/人物简介');

    if (self.人物组[num].性别 == '男') {
      node.getChildByName('头像').getComponent(cc.Sprite).spriteFrame = cc.find('game').getComponent('game').男.getSpriteFrame(self.人物组[num].头像);
    } else {
      node.getChildByName('头像').getComponent(cc.Sprite).spriteFrame = cc.find('game').getComponent('game').女.getSpriteFrame(self.人物组[num].头像);
    }

    cc.find('Canvas/人物简介/基础/人物名字').getComponent(cc.Label).string = self.人物组[num].名字;
    cc.find('Canvas/人物简介/基础/人物性别').getComponent(cc.Label).string = self.人物组[num].性别;
    cc.find('Canvas/人物简介/基础/人物年龄').getComponent(cc.Label).string = self.人物组[num].年龄 + '月';
    cc.find('Canvas/人物简介/基础/人物状态').getComponent(cc.Label).string = self.人物组[num].状态;
    cc.find('Canvas/人物简介/基础/人物优点').getComponent(cc.Label).string = self.人物组[num].优点;

    if (self.时间 >= 5) {
      cc.find('Canvas/人物简介/基础/人物缺点').getComponent(cc.Label).string = self.人物组[num].缺点;
    } //属性


    cc.find('Canvas/人物简介/属性/力量').getComponent(cc.Label).string = self.人物组[num].力量;
    cc.find('Canvas/人物简介/属性/智力').getComponent(cc.Label).string = self.人物组[num].智力;
    cc.find('Canvas/人物简介/属性/耐力').getComponent(cc.Label).string = self.人物组[num].耐力;
    cc.find('Canvas/人物简介/属性/专注').getComponent(cc.Label).string = self.人物组[num].专注;
    cc.find('Canvas/人物简介/属性/幸运').getComponent(cc.Label).string = self.人物组[num].幸运;
    cc.find('Canvas/人物简介/属性/气质').getComponent(cc.Label).string = self.人物组[num].气质;
    cc.find('Canvas/人物简介').active = true;
    cc.find('Canvas/人物简介').getComponent(cc.Animation).play();
  },
  touchEnd: function touchEnd() {
    cc.find('Canvas/人物简介').active = false;
  }
});

cc._RF.pop();