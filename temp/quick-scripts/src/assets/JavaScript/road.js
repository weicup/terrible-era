"use strict";
cc._RF.push(module, 'b16cbVUMO9GkLyWEgjZfVii', 'road');
// JavaScript/road.js

"use strict";

// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
cc.Class({
  "extends": cc.Component,
  properties: {},
  onLoad: function onLoad() {
    cc.director.preloadScene('login', this.onProgress, function () {
      cc.find('Canvas/进度条状态').getComponent(cc.Label).string = '加载完成';
      setTimeout(function () {
        cc.director.loadScene('game');
      }, 1000);
    });
  },
  onProgress: function onProgress(completed, total) {
    cc.find('Canvas/进度条/进度条百分比').getComponent(cc.Label).string = Math.floor(completed / total) * 100 + '%';
    cc.find('Canvas/进度条').getComponent(cc.ProgressBar).progress = completed / total;
  }
});

cc._RF.pop();