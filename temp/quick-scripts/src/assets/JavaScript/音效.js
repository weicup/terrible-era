"use strict";
cc._RF.push(module, '8e1e2frglxIe42clbWZGA2O', '音效');
// JavaScript/音效.js

"use strict";

// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
cc.Class({
  "extends": cc.Component,
  properties: {
    node1: cc.AudioClip,
    node2: cc.AudioClip,
    node3: cc.AudioClip,
    node4: cc.AudioClip
  },
  背景音乐: function _() {
    cc.audioEngine.play(this.node1, true, all.volume);
  },
  按钮: function _() {
    cc.audioEngine.play(this.node2, false, all.volume);
  },
  金币: function _() {
    cc.audioEngine.play(this.node3, false, all.volume);
  }
});

cc._RF.pop();