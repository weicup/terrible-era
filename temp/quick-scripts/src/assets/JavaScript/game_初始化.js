"use strict";
cc._RF.push(module, 'e6219GVvKdNA4/ip5vKZUcq', 'game_初始化');
// JavaScript/game_初始化.js

"use strict";

// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
cc.Class({
  "extends": cc.Component,
  properties: {
    game_男人物: cc.SpriteAtlas,
    game_女人物: cc.SpriteAtlas,
    game_选择人物: cc.Prefab,
    game_工作面板人物: cc.Prefab,
    game_日程列表: cc.Prefab,
    game_日程列表_其他: cc.Prefab
  },
  onLoad: function onLoad() {
    //初始化预选人物
    for (var i = 0; i < 10; i++) {
      var sex;

      if (i < 5) {
        sex = 1;
      } else {
        sex = 0;
      }

      if (sex == 1) {
        var name2 = all.男名[Math.floor(Math.random() * all.男名.length)];
        var head = Math.floor(Math.random() * 17 + 1);
        var 属性 = [11, 1, 11, 1];
      } else {
        var name2 = all.女名[Math.floor(Math.random() * all.女名.length)];
        var head = Math.floor(Math.random() * 14 + 1);
        var 属性 = [1, 1, 1, 1];
      }

      for (var j = 0; j < 236; j++) {
        属性[Math.floor(Math.random() * 4)]++;
      }

      var name1 = all.姓[Math.floor(Math.random() * all.姓.length)];
      var obj = {
        名字: name1 + name2,
        性别: sex,
        力量: 属性[0],
        智力: 属性[1],
        技巧: 属性[2],
        运气: 属性[3],
        头像: head,
        优点: all.优点[Math.floor(Math.random() * all.优点.length, 1)],
        缺点: '未知',
        状态: '正常',
        工作: '空闲',
        年龄: 0,
        收获量: 0,
        技能: {
          采集: [0.00, 1],
          //经验值和等级
          狩猎: [0.00, 1],
          砍伐: [0.00, 1],
          挖掘: [0.00, 1],
          制造: [0.00, 1],
          研究: [0.00, 1],
          祈祷: [0.00, 1]
        },
        buff: {
          怀孕: 0,
          感冒: 0,
          受伤: 0
        }
      };
      all.prerole.push(obj);
      var node = cc.instantiate(this.game_选择人物);
      node.name = i + '';

      if (obj.性别 == '1') {
        node.getChildByName('头像').getComponent(cc.Sprite).spriteFrame = this.game_男人物.getSpriteFrame(obj.头像);
      } else {
        node.getChildByName('头像').getComponent(cc.Sprite).spriteFrame = this.game_女人物.getSpriteFrame(obj.头像);
      }

      node.getChildByName('名字').getComponent(cc.Label).string = obj.名字;
      node.parent = cc.find('Canvas/选择界面/选择人物');
    }
  },
  日程过渡: function _() {
    var _this = this;

    cc.find('Canvas').pauseSystemEvents(true);
    cc.find('Canvas/黑屏').getComponent(cc.Animation).play();

    if (all.main.time != 0) {
      cc.find('Canvas').getComponent(cc.Animation).play('界面消失');
    }

    setTimeout(function () {
      cc.find('Canvas/黑屏').getComponent(cc.Animation).play('白屏');
      setTimeout(function () {
        cc.find('Canvas').getComponent(cc.Animation).play();
        setTimeout(function () {
          cc.find('Canvas/一级遮罩').active = true;
          cc.find('Canvas/日程面板').active = true;
          cc.find('Canvas/日程面板').getComponent(cc.Animation).play();
          cc.find('Canvas').resumeSystemEvents(true);
        }, 2000);
      }, 1000);

      _this.主逻辑();
    }, 2600);
  },
  主逻辑: function _() {
    console.log('开始处理主逻辑');
    this.重置所有节点();
    this.处理工作();
    this.处理时间(); //处理完毕,开始回到主界面

    cc.find('Canvas/详情界面/详情').active = false;
    cc.find('Canvas/人物界面/人物列表/view/选中框').active = false;
    console.log(all.role);
  },
  重置所有节点: function _() {
    var child = cc.find('Canvas/日程面板/内容/view/content').children;

    for (var i = 0; i < child.length; i++) {
      //显示所有事件的节点
      child[i].active = true;

      if (i < 6) {
        child[i].getChildByName('总收获').getChildByName('收获量').getComponent(cc.Label).string = 0;
      } //删除日程面板原有人物数据


      var child2 = child[i].children;

      for (var j = 0; j < child2.length; j++) {
        if (child2[j].name == 'game_日程列表' || child2[j].name == 'game_日程列表_其他') {
          child2[j].destroy();
        }
      }
    }

    cc.find('Canvas/日程面板/内容/view/content/空闲/总消耗/消耗量').getComponent(cc.Label).string = 0;
  },
  处理工作: function _() {
    //处理工作
    for (var i = 0; i < all.role.length; i++) {
      var 倍数 = 1;

      switch (all.role[i].工作) {
        case '空闲':
          if (all.role[i].性别 == 0 && all.role[i].状态 != '怀孕') {
            //女生空闲
            var num = Math.floor(Math.random() * 100 + 1);

            if (num <= 20) {
              cc.find('Canvas/人物界面/人物列表/view/content/' + i + '/状态').getComponent(cc.Label).string = '怀孕';
              all.role[i].状态 = '怀孕';
              all.role[i].收获量 = '怀孕了';
            } else {
              all.role[i].收获量 = '无所事事';
            }
          } else {
            //男生空闲
            all.role[i].收获量 = '无所事事';
          }

          break;

        case '采集':
          if (all.main.凶吉 == '硕果累累') {
            倍数 += 0.5;
          }

          var num = Math.random() * 0.5 + 1;
          all.role[i].收获量 = ((num + all.role[i].技能.采集[1] * all.role[i].技巧 * 0.01) * 倍数).toFixed(2); //优点_好学

          倍数 = 1;

          if (all.role[i].优点 == '好学') {
            倍数 += 0.2;
          }

          all.role[i].技能.采集[0] = ((all.role[i].技能.采集[0] + all.role[i].收获量 * 1) * 倍数).toFixed(2) * 1;

          if (all.role[i].技能.采集[0] >= all.role[i].技能.采集[1] * 10) {
            cc.find('code').getComponent('game').消息提示(all.role[i].名字 + '的' + all.role[i].工作 + '技能升级了');
            all.role[i].技能.采集[0] = all.role[i].技能.采集[0] - all.role[i].技能.采集[1] * 10;
            all.role[i].技能.采集[1]++;
          }

          break;

        case '狩猎':
          var 倍数;
          var success = Math.floor(all.role[i].技巧 / 2 + (all.role[i].技能.狩猎[1] - 1) * 5);

          if (all.main.凶吉 = '漫山遍野') {
            success = success + 10;
          }

          var randomNum = Math.floor(Math.random() * 100 + 1);

          if (all.role[i].优点 == '好学') {
            倍数 = 1.2;
          } else {
            倍数 = 1;
          }

          if (randomNum <= success) {
            all.role[i].收获量 = 6;
            all.role[i].技能.狩猎[0] = ((all.role[i].技能.狩猎[0] * 1 + all.role[i].收获量 * 1) * 倍数).toFixed(2) * 1;
          } else {
            all.role[i].收获量 = 0;
            all.role[i].技能.狩猎[0] = (all.role[i].技能.狩猎[0] * 1 + 2).toFixed(2) * 1;
          }

          if (all.role[i].技能.狩猎[0] >= all.role[i].技能.狩猎[1] * 10) {
            cc.find('code').getComponent('game').消息提示(all.role[i].名字 + '的' + all.role[i].工作 + '技能升级了');
            all.role[i].技能.狩猎[0] = all.role[i].技能.狩猎[0] - all.role[i].技能.狩猎[1] * 10;
            all.role[i].技能.狩猎[1]++;
          }

          break;
      } //放置节点到日程板上


      if (all.role[i].工作 == '空闲') {
        var node = cc.instantiate(this.game_日程列表_其他);
      } else {
        var node = cc.instantiate(this.game_日程列表);
      }

      if (all.role[i].性别 == 1) {
        node.getChildByName('头像').getComponent(cc.Sprite).spriteFrame = this.game_男人物.getSpriteFrame(all.role[i].头像);
      } else {
        node.getChildByName('头像').getComponent(cc.Sprite).spriteFrame = this.game_女人物.getSpriteFrame(all.role[i].头像);
      }

      node.getChildByName('名字').getComponent(cc.Label).string = all.role[i].名字;
      node.getChildByName('收获量').getComponent(cc.Label).string = all.role[i].收获量;

      if (all.role[i].工作 != '空闲') {
        cc.find('Canvas/日程面板/内容/view/content/' + all.role[i].工作 + '/总收获/收获量').getComponent(cc.Label).string = (cc.find('Canvas/日程面板/内容/view/content/' + all.role[i].工作 + '/总收获/收获量').getComponent(cc.Label).string * 1 + all.role[i].收获量 * 1).toFixed(2);
      }

      var num = Math.random() * 0.4 + 0.8;

      if (all.role[i].优点 == '小鸟胃') {
        num = num / 2;
      }

      cc.find('Canvas/日程面板/内容/view/content/空闲/总消耗/消耗量').getComponent(cc.Label).string = (cc.find('Canvas/日程面板/内容/view/content/空闲/总消耗/消耗量').getComponent(cc.Label).string * 1 + num).toFixed(2);
      node.parent = cc.find('Canvas/日程面板/内容/view/content').getChildByName(all.role[i].工作);
    } //工作结算


    var food = cc.find('Canvas/上界面/食物').getComponent(cc.Label).string;
    console.log(food);
    food = food * 1 + cc.find('Canvas/日程面板/内容/view/content/采集/总收获/收获量').getComponent(cc.Label).string * 1;
    console.log(food);
    food = food * 1 + cc.find('Canvas/日程面板/内容/view/content/狩猎/总收获/收获量').getComponent(cc.Label).string * 1;
    console.log(food);
    food = (food * 1 - cc.find('Canvas/日程面板/内容/view/content/空闲/总消耗/消耗量').getComponent(cc.Label).string * 1).toFixed(2);
    console.log(food);
    cc.find('Canvas/上界面/食物').getComponent(cc.Label).string = food; //节点还没销毁，操。这里耽误了老子两个小时。到最后做了大量测试才知道原来destroy()并不是立刻销毁，要下一帧(16ms)才会。所以这时候节点还在。

    setTimeout(function () {
      //若工作事件没有,则隐藏
      var child = cc.find('Canvas/日程面板/内容/view/content').children;

      for (var i = 0; i < child.length - 1; i++) {
        if (child[i].childrenCount == 3) {
          child[i].active = false;
        }
      }
    }, 50);
  },
  处理时间: function _() {
    //处理时间
    all.main.time++;
    all.main.year = Math.floor(all.main.time / 12);
    all.main.month = all.main.time % 12;

    if (all.main.month == 0) {
      all.main.month = 12;
    }

    if (all.main.year.isInteger) {
      all.main.year--;
    }

    if (all.main.month <= 3) {
      if (all.main.month == 1) {
        setTimeout(function () {
          cc.find('code').getComponent('game').消息提示('春天来了');
        }, 3000);
      }

      all.main.season = '春';
    } else if (all.main.month > 3 && all.main.month <= 6) {
      if (all.main.month == 4) {
        setTimeout(function () {
          cc.find('code').getComponent('game').消息提示('夏天来了');
        }, 3000);
      }

      all.main.season = '夏';
    } else if (all.main.month > 6 && all.main.month <= 9) {
      if (all.main.month == 7) {
        setTimeout(function () {
          cc.find('code').getComponent('game').消息提示('秋天来了');
        }, 3000);
      }

      all.main.season = '秋';
    } else {
      if (all.main.month == 10) {
        setTimeout(function () {
          cc.find('code').getComponent('game').消息提示('冬天来了');
        }, 3000);
      }

      all.main.season = '冬';
    }

    cc.find('Canvas/上界面/环境/时间').getComponent(cc.Label).string = all.main.year + '年' + all.main.month + '月 ' + all.main.season; //处理凶吉

    if (all.main.time == 1) {
      all.main.凶吉 = '平安无事';
    } else {
      var num = Math.floor(Math.random() * 100 + 1);

      switch (all.main.season) {
        case '春':
          if (num <= 30) {
            all.main.凶吉 = '平安无事';
          } else if (num > 30 && num <= 50) {
            all.main.凶吉 = '硕果累累';
          } else if (num > 50 && num <= 70) {
            all.main.凶吉 = '漫山遍野';
          } else if (num > 70 && num <= 90) {
            all.main.凶吉 = '繁殖季节';
          } else {
            all.main.凶吉 = '流感';
          }

          break;

        case '夏':
          break;

        case '秋':
          break;

        case '冬':
          break;
      }
    }

    cc.find('Canvas/上界面/环境/吉凶').getComponent(cc.Label).string = all.main.凶吉;
  }
});

cc._RF.pop();