
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/__qc_index__.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}
require('./assets/JavaScript/game');
require('./assets/JavaScript/game/人物简介');
require('./assets/JavaScript/game/日程更新');
require('./assets/JavaScript/game/聊天换行');
require('./assets/JavaScript/game_初始化');
require('./assets/JavaScript/login');
require('./assets/JavaScript/road');
require('./assets/JavaScript/全局变量');
require('./assets/JavaScript/聊天内容');
require('./assets/JavaScript/音效');

                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();