
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/JavaScript/聊天内容.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'ed1a6UtoJVLI69JH5xXmvO+', '聊天内容');
// JavaScript/聊天内容.js

"use strict";

// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
cc.Class({
  "extends": cc.Component,
  properties: {
    自己: cc.Prefab,
    别人: cc.Prefab,
    系统: cc.Prefab,
    普通聊天: cc.Prefab,
    聊天框: cc.SpriteAtlas,
    node1: cc.Node
  },
  onLoad: function onLoad() {}
});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcSmF2YVNjcmlwdFxc6IGK5aSp5YaF5a65LmpzIl0sIm5hbWVzIjpbImNjIiwiQ2xhc3MiLCJDb21wb25lbnQiLCJwcm9wZXJ0aWVzIiwi6Ieq5bexIiwiUHJlZmFiIiwi5Yir5Lq6Iiwi57O757ufIiwi5pmu6YCa6IGK5aSpIiwi6IGK5aSp5qGGIiwiU3ByaXRlQXRsYXMiLCJub2RlMSIsIk5vZGUiLCJvbkxvYWQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUFBLEVBQUUsQ0FBQ0MsS0FBSCxDQUFTO0FBQ0wsYUFBU0QsRUFBRSxDQUFDRSxTQURQO0FBR0xDLEVBQUFBLFVBQVUsRUFBRTtBQUNSQyxJQUFBQSxFQUFFLEVBQUVKLEVBQUUsQ0FBQ0ssTUFEQztBQUVSQyxJQUFBQSxFQUFFLEVBQUVOLEVBQUUsQ0FBQ0ssTUFGQztBQUdSRSxJQUFBQSxFQUFFLEVBQUVQLEVBQUUsQ0FBQ0ssTUFIQztBQUlSRyxJQUFBQSxJQUFJLEVBQUVSLEVBQUUsQ0FBQ0ssTUFKRDtBQUtSSSxJQUFBQSxHQUFHLEVBQUVULEVBQUUsQ0FBQ1UsV0FMQTtBQU1SQyxJQUFBQSxLQUFLLEVBQUVYLEVBQUUsQ0FBQ1k7QUFORixHQUhQO0FBV0xDLEVBQUFBLE1BWEssb0JBV0ksQ0FFUjtBQWJJLENBQVQiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8vIExlYXJuIGNjLkNsYXNzOlxyXG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9jbGFzcy5odG1sXHJcbi8vIExlYXJuIEF0dHJpYnV0ZTpcclxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxyXG4vLyBMZWFybiBsaWZlLWN5Y2xlIGNhbGxiYWNrczpcclxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvbGlmZS1jeWNsZS1jYWxsYmFja3MuaHRtbFxyXG5cclxuY2MuQ2xhc3Moe1xyXG4gICAgZXh0ZW5kczogY2MuQ29tcG9uZW50LFxyXG5cclxuICAgIHByb3BlcnRpZXM6IHtcclxuICAgICAgICDoh6rlt7E6IGNjLlByZWZhYixcclxuICAgICAgICDliKvkuro6IGNjLlByZWZhYixcclxuICAgICAgICDns7vnu586IGNjLlByZWZhYixcclxuICAgICAgICDmma7pgJrogYrlpKk6IGNjLlByZWZhYixcclxuICAgICAgICDogYrlpKnmoYY6IGNjLlNwcml0ZUF0bGFzLFxyXG4gICAgICAgIG5vZGUxOiBjYy5Ob2RlLFxyXG4gICAgfSxcclxuICAgIG9uTG9hZCgpIHtcclxuXHJcbiAgICB9XHJcblxyXG59KTtcclxuIl19