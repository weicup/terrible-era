
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/JavaScript/音效.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '8e1e2frglxIe42clbWZGA2O', '音效');
// JavaScript/音效.js

"use strict";

// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
cc.Class({
  "extends": cc.Component,
  properties: {
    node1: cc.AudioClip,
    node2: cc.AudioClip,
    node3: cc.AudioClip,
    node4: cc.AudioClip
  },
  背景音乐: function _() {
    cc.audioEngine.play(this.node1, true, all.volume);
  },
  按钮: function _() {
    cc.audioEngine.play(this.node2, false, all.volume);
  },
  金币: function _() {
    cc.audioEngine.play(this.node3, false, all.volume);
  }
});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcSmF2YVNjcmlwdFxc6Z+z5pWILmpzIl0sIm5hbWVzIjpbImNjIiwiQ2xhc3MiLCJDb21wb25lbnQiLCJwcm9wZXJ0aWVzIiwibm9kZTEiLCJBdWRpb0NsaXAiLCJub2RlMiIsIm5vZGUzIiwibm9kZTQiLCLog4zmma/pn7PkuZAiLCJhdWRpb0VuZ2luZSIsInBsYXkiLCJhbGwiLCJ2b2x1bWUiLCLmjInpkq4iLCLph5HluIEiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUFBLEVBQUUsQ0FBQ0MsS0FBSCxDQUFTO0FBQ0wsYUFBU0QsRUFBRSxDQUFDRSxTQURQO0FBR0xDLEVBQUFBLFVBQVUsRUFBRTtBQUNSQyxJQUFBQSxLQUFLLEVBQUVKLEVBQUUsQ0FBQ0ssU0FERjtBQUVSQyxJQUFBQSxLQUFLLEVBQUVOLEVBQUUsQ0FBQ0ssU0FGRjtBQUdSRSxJQUFBQSxLQUFLLEVBQUVQLEVBQUUsQ0FBQ0ssU0FIRjtBQUlSRyxJQUFBQSxLQUFLLEVBQUVSLEVBQUUsQ0FBQ0s7QUFKRixHQUhQO0FBU0xJLEVBQUFBLElBVEssZUFTRTtBQUNIVCxJQUFBQSxFQUFFLENBQUNVLFdBQUgsQ0FBZUMsSUFBZixDQUFvQixLQUFLUCxLQUF6QixFQUFnQyxJQUFoQyxFQUFzQ1EsR0FBRyxDQUFDQyxNQUExQztBQUNILEdBWEk7QUFZTEMsRUFBQUEsRUFaSyxlQVlBO0FBQ0RkLElBQUFBLEVBQUUsQ0FBQ1UsV0FBSCxDQUFlQyxJQUFmLENBQW9CLEtBQUtMLEtBQXpCLEVBQWdDLEtBQWhDLEVBQXVDTSxHQUFHLENBQUNDLE1BQTNDO0FBQ0gsR0FkSTtBQWVMRSxFQUFBQSxFQWZLLGVBZUE7QUFDRGYsSUFBQUEsRUFBRSxDQUFDVSxXQUFILENBQWVDLElBQWYsQ0FBb0IsS0FBS0osS0FBekIsRUFBZ0MsS0FBaEMsRUFBdUNLLEdBQUcsQ0FBQ0MsTUFBM0M7QUFDSDtBQWpCSSxDQUFUIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyIvLyBMZWFybiBjYy5DbGFzczpcclxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvY2xhc3MuaHRtbFxyXG4vLyBMZWFybiBBdHRyaWJ1dGU6XHJcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcclxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XHJcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcclxuXHJcbmNjLkNsYXNzKHtcclxuICAgIGV4dGVuZHM6IGNjLkNvbXBvbmVudCxcclxuXHJcbiAgICBwcm9wZXJ0aWVzOiB7XHJcbiAgICAgICAgbm9kZTE6IGNjLkF1ZGlvQ2xpcCxcclxuICAgICAgICBub2RlMjogY2MuQXVkaW9DbGlwLFxyXG4gICAgICAgIG5vZGUzOiBjYy5BdWRpb0NsaXAsXHJcbiAgICAgICAgbm9kZTQ6IGNjLkF1ZGlvQ2xpcCxcclxuICAgIH0sXHJcbiAgICDog4zmma/pn7PkuZAoKSB7XHJcbiAgICAgICAgY2MuYXVkaW9FbmdpbmUucGxheSh0aGlzLm5vZGUxLCB0cnVlLCBhbGwudm9sdW1lKTtcclxuICAgIH0sXHJcbiAgICDmjInpkq4oKSB7XHJcbiAgICAgICAgY2MuYXVkaW9FbmdpbmUucGxheSh0aGlzLm5vZGUyLCBmYWxzZSwgYWxsLnZvbHVtZSk7XHJcbiAgICB9LFxyXG4gICAg6YeR5biBKCkge1xyXG4gICAgICAgIGNjLmF1ZGlvRW5naW5lLnBsYXkodGhpcy5ub2RlMywgZmFsc2UsIGFsbC52b2x1bWUpO1xyXG4gICAgfSxcclxuXHJcbn0pO1xyXG4iXX0=