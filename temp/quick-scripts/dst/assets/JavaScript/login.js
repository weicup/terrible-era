
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/JavaScript/login.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '8e189UpSGBPBo7lb888pxOG', 'login');
// JavaScript/login.js

"use strict";

cc.Class({
  "extends": cc.Component,
  properties: {
    node1: cc.Node,
    node2: cc.Node,
    node3: cc.Node,
    atlas: cc.SpriteAtlas,
    玩家列表: cc.Prefab,
    media_error: cc.AudioClip
  },
  onLoad: function onLoad() {
    window.all = require('./全局变量');
    cc.find('code').getComponent('音效').背景音乐();

    if (cc.sys.localStorage.getItem('game_num') != null) {
      cc.find('Canvas/战绩/游玩总代数').getComponent(cc.Label).string = cc.sys.localStorage.getItem('game_num');
    } else {
      cc.find('Canvas/战绩/游玩总代数').getComponent(cc.Label).string = '0代';
    }

    if (cc.sys.localStorage.getItem('game_all') != null) {
      cc.find('Canvas/战绩/存活总月数').getComponent(cc.Label).string = cc.sys.localStorage.getItem('game_all');
    } else {
      cc.find('Canvas/战绩/存活总月数').getComponent(cc.Label).string = '0月';
    }

    if (cc.sys.localStorage.getItem('game_average') != null) {
      cc.find('Canvas/战绩/平均存活数').getComponent(cc.Label).string = cc.sys.localStorage.getItem('game_average');
    } else {
      cc.find('Canvas/战绩/平均存活数').getComponent(cc.Label).string = '0月';
    }
  },
  开关界面: function _(target, data) {
    if (data == '开') {
      cc.find('Canvas/设置界面').active = true;
      cc.find('Canvas/一级蒙版').active = true;
      cc.director.loadScene('road');
    } else {
      cc.find('Canvas/设置界面').active = false;
      cc.find('Canvas/一级蒙版').active = false;
    }
  },
  消息提示: function _(msg) {
    cc.audioEngine.play(this.media_error, false, all.volume);
    cc.find('Canvas/消息背景板/消息').getComponent(cc.Label).string = msg;
    cc.find('Canvas/消息背景板').getComponent(cc.Animation).play();
  },
  模式加减: function _(target, data) {}
});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcSmF2YVNjcmlwdFxcbG9naW4uanMiXSwibmFtZXMiOlsiY2MiLCJDbGFzcyIsIkNvbXBvbmVudCIsInByb3BlcnRpZXMiLCJub2RlMSIsIk5vZGUiLCJub2RlMiIsIm5vZGUzIiwiYXRsYXMiLCJTcHJpdGVBdGxhcyIsIueOqeWutuWIl+ihqCIsIlByZWZhYiIsIm1lZGlhX2Vycm9yIiwiQXVkaW9DbGlwIiwib25Mb2FkIiwid2luZG93IiwiYWxsIiwicmVxdWlyZSIsImZpbmQiLCJnZXRDb21wb25lbnQiLCLog4zmma/pn7PkuZAiLCJzeXMiLCJsb2NhbFN0b3JhZ2UiLCJnZXRJdGVtIiwiTGFiZWwiLCJzdHJpbmciLCLlvIDlhbPnlYzpnaIiLCJ0YXJnZXQiLCJkYXRhIiwiYWN0aXZlIiwiZGlyZWN0b3IiLCJsb2FkU2NlbmUiLCLmtojmga/mj5DnpLoiLCJtc2ciLCJhdWRpb0VuZ2luZSIsInBsYXkiLCJ2b2x1bWUiLCJBbmltYXRpb24iLCLmqKHlvI/liqDlh48iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUFBLEVBQUUsQ0FBQ0MsS0FBSCxDQUFTO0FBQ0wsYUFBU0QsRUFBRSxDQUFDRSxTQURQO0FBRUxDLEVBQUFBLFVBQVUsRUFBRTtBQUNSQyxJQUFBQSxLQUFLLEVBQUVKLEVBQUUsQ0FBQ0ssSUFERjtBQUVSQyxJQUFBQSxLQUFLLEVBQUVOLEVBQUUsQ0FBQ0ssSUFGRjtBQUdSRSxJQUFBQSxLQUFLLEVBQUVQLEVBQUUsQ0FBQ0ssSUFIRjtBQUlSRyxJQUFBQSxLQUFLLEVBQUVSLEVBQUUsQ0FBQ1MsV0FKRjtBQUtSQyxJQUFBQSxJQUFJLEVBQUVWLEVBQUUsQ0FBQ1csTUFMRDtBQU1SQyxJQUFBQSxXQUFXLEVBQUVaLEVBQUUsQ0FBQ2E7QUFOUixHQUZQO0FBVUxDLEVBQUFBLE1BVkssb0JBVUk7QUFDTEMsSUFBQUEsTUFBTSxDQUFDQyxHQUFQLEdBQWFDLE9BQU8sQ0FBQyxRQUFELENBQXBCO0FBQ0FqQixJQUFBQSxFQUFFLENBQUNrQixJQUFILENBQVEsTUFBUixFQUFnQkMsWUFBaEIsQ0FBNkIsSUFBN0IsRUFBbUNDLElBQW5DOztBQUNBLFFBQUlwQixFQUFFLENBQUNxQixHQUFILENBQU9DLFlBQVAsQ0FBb0JDLE9BQXBCLENBQTRCLFVBQTVCLEtBQTJDLElBQS9DLEVBQXFEO0FBQ2pEdkIsTUFBQUEsRUFBRSxDQUFDa0IsSUFBSCxDQUFRLGlCQUFSLEVBQTJCQyxZQUEzQixDQUF3Q25CLEVBQUUsQ0FBQ3dCLEtBQTNDLEVBQWtEQyxNQUFsRCxHQUEyRHpCLEVBQUUsQ0FBQ3FCLEdBQUgsQ0FBT0MsWUFBUCxDQUFvQkMsT0FBcEIsQ0FBNEIsVUFBNUIsQ0FBM0Q7QUFDSCxLQUZELE1BRU87QUFDSHZCLE1BQUFBLEVBQUUsQ0FBQ2tCLElBQUgsQ0FBUSxpQkFBUixFQUEyQkMsWUFBM0IsQ0FBd0NuQixFQUFFLENBQUN3QixLQUEzQyxFQUFrREMsTUFBbEQsR0FBMkQsSUFBM0Q7QUFDSDs7QUFDRCxRQUFJekIsRUFBRSxDQUFDcUIsR0FBSCxDQUFPQyxZQUFQLENBQW9CQyxPQUFwQixDQUE0QixVQUE1QixLQUEyQyxJQUEvQyxFQUFxRDtBQUNqRHZCLE1BQUFBLEVBQUUsQ0FBQ2tCLElBQUgsQ0FBUSxpQkFBUixFQUEyQkMsWUFBM0IsQ0FBd0NuQixFQUFFLENBQUN3QixLQUEzQyxFQUFrREMsTUFBbEQsR0FBMkR6QixFQUFFLENBQUNxQixHQUFILENBQU9DLFlBQVAsQ0FBb0JDLE9BQXBCLENBQTRCLFVBQTVCLENBQTNEO0FBQ0gsS0FGRCxNQUVPO0FBQ0h2QixNQUFBQSxFQUFFLENBQUNrQixJQUFILENBQVEsaUJBQVIsRUFBMkJDLFlBQTNCLENBQXdDbkIsRUFBRSxDQUFDd0IsS0FBM0MsRUFBa0RDLE1BQWxELEdBQTJELElBQTNEO0FBQ0g7O0FBQ0QsUUFBSXpCLEVBQUUsQ0FBQ3FCLEdBQUgsQ0FBT0MsWUFBUCxDQUFvQkMsT0FBcEIsQ0FBNEIsY0FBNUIsS0FBK0MsSUFBbkQsRUFBeUQ7QUFDckR2QixNQUFBQSxFQUFFLENBQUNrQixJQUFILENBQVEsaUJBQVIsRUFBMkJDLFlBQTNCLENBQXdDbkIsRUFBRSxDQUFDd0IsS0FBM0MsRUFBa0RDLE1BQWxELEdBQTJEekIsRUFBRSxDQUFDcUIsR0FBSCxDQUFPQyxZQUFQLENBQW9CQyxPQUFwQixDQUE0QixjQUE1QixDQUEzRDtBQUNILEtBRkQsTUFFTztBQUNIdkIsTUFBQUEsRUFBRSxDQUFDa0IsSUFBSCxDQUFRLGlCQUFSLEVBQTJCQyxZQUEzQixDQUF3Q25CLEVBQUUsQ0FBQ3dCLEtBQTNDLEVBQWtEQyxNQUFsRCxHQUEyRCxJQUEzRDtBQUNIO0FBQ0osR0E1Qkk7QUE2QkxDLEVBQUFBLElBN0JLLGFBNkJBQyxNQTdCQSxFQTZCUUMsSUE3QlIsRUE2QmM7QUFDZixRQUFJQSxJQUFJLElBQUksR0FBWixFQUFpQjtBQUNiNUIsTUFBQUEsRUFBRSxDQUFDa0IsSUFBSCxDQUFRLGFBQVIsRUFBdUJXLE1BQXZCLEdBQWdDLElBQWhDO0FBQ0E3QixNQUFBQSxFQUFFLENBQUNrQixJQUFILENBQVEsYUFBUixFQUF1QlcsTUFBdkIsR0FBZ0MsSUFBaEM7QUFDQTdCLE1BQUFBLEVBQUUsQ0FBQzhCLFFBQUgsQ0FBWUMsU0FBWixDQUFzQixNQUF0QjtBQUNILEtBSkQsTUFJTztBQUNIL0IsTUFBQUEsRUFBRSxDQUFDa0IsSUFBSCxDQUFRLGFBQVIsRUFBdUJXLE1BQXZCLEdBQWdDLEtBQWhDO0FBQ0E3QixNQUFBQSxFQUFFLENBQUNrQixJQUFILENBQVEsYUFBUixFQUF1QlcsTUFBdkIsR0FBZ0MsS0FBaEM7QUFDSDtBQUNKLEdBdENJO0FBdUNMRyxFQUFBQSxJQXZDSyxhQXVDQUMsR0F2Q0EsRUF1Q0s7QUFDTmpDLElBQUFBLEVBQUUsQ0FBQ2tDLFdBQUgsQ0FBZUMsSUFBZixDQUFvQixLQUFLdkIsV0FBekIsRUFBc0MsS0FBdEMsRUFBNkNJLEdBQUcsQ0FBQ29CLE1BQWpEO0FBQ0FwQyxJQUFBQSxFQUFFLENBQUNrQixJQUFILENBQVEsaUJBQVIsRUFBMkJDLFlBQTNCLENBQXdDbkIsRUFBRSxDQUFDd0IsS0FBM0MsRUFBa0RDLE1BQWxELEdBQTJEUSxHQUEzRDtBQUNBakMsSUFBQUEsRUFBRSxDQUFDa0IsSUFBSCxDQUFRLGNBQVIsRUFBd0JDLFlBQXhCLENBQXFDbkIsRUFBRSxDQUFDcUMsU0FBeEMsRUFBbURGLElBQW5EO0FBQ0gsR0EzQ0k7QUE0Q0xHLEVBQUFBLElBNUNLLGFBNENBWCxNQTVDQSxFQTRDUUMsSUE1Q1IsRUE0Q2MsQ0FFbEI7QUE5Q0ksQ0FBVCIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiY2MuQ2xhc3Moe1xyXG4gICAgZXh0ZW5kczogY2MuQ29tcG9uZW50LFxyXG4gICAgcHJvcGVydGllczoge1xyXG4gICAgICAgIG5vZGUxOiBjYy5Ob2RlLFxyXG4gICAgICAgIG5vZGUyOiBjYy5Ob2RlLFxyXG4gICAgICAgIG5vZGUzOiBjYy5Ob2RlLFxyXG4gICAgICAgIGF0bGFzOiBjYy5TcHJpdGVBdGxhcyxcclxuICAgICAgICDnjqnlrrbliJfooag6IGNjLlByZWZhYixcclxuICAgICAgICBtZWRpYV9lcnJvcjogY2MuQXVkaW9DbGlwLFxyXG4gICAgfSxcclxuICAgIG9uTG9hZCgpIHtcclxuICAgICAgICB3aW5kb3cuYWxsID0gcmVxdWlyZSgnLi/lhajlsYDlj5jph48nKTtcclxuICAgICAgICBjYy5maW5kKCdjb2RlJykuZ2V0Q29tcG9uZW50KCfpn7PmlYgnKS7og4zmma/pn7PkuZAoKTtcclxuICAgICAgICBpZiAoY2Muc3lzLmxvY2FsU3RvcmFnZS5nZXRJdGVtKCdnYW1lX251bScpICE9IG51bGwpIHtcclxuICAgICAgICAgICAgY2MuZmluZCgnQ2FudmFzL+aImOe7qS/muLjnjqnmgLvku6PmlbAnKS5nZXRDb21wb25lbnQoY2MuTGFiZWwpLnN0cmluZyA9IGNjLnN5cy5sb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnZ2FtZV9udW0nKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjYy5maW5kKCdDYW52YXMv5oiY57upL+a4uOeOqeaAu+S7o+aVsCcpLmdldENvbXBvbmVudChjYy5MYWJlbCkuc3RyaW5nID0gJzDku6MnO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoY2Muc3lzLmxvY2FsU3RvcmFnZS5nZXRJdGVtKCdnYW1lX2FsbCcpICE9IG51bGwpIHtcclxuICAgICAgICAgICAgY2MuZmluZCgnQ2FudmFzL+aImOe7qS/lrZjmtLvmgLvmnIjmlbAnKS5nZXRDb21wb25lbnQoY2MuTGFiZWwpLnN0cmluZyA9IGNjLnN5cy5sb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnZ2FtZV9hbGwnKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjYy5maW5kKCdDYW52YXMv5oiY57upL+WtmOa0u+aAu+aciOaVsCcpLmdldENvbXBvbmVudChjYy5MYWJlbCkuc3RyaW5nID0gJzDmnIgnO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoY2Muc3lzLmxvY2FsU3RvcmFnZS5nZXRJdGVtKCdnYW1lX2F2ZXJhZ2UnKSAhPSBudWxsKSB7XHJcbiAgICAgICAgICAgIGNjLmZpbmQoJ0NhbnZhcy/miJjnu6kv5bmz5Z2H5a2Y5rS75pWwJykuZ2V0Q29tcG9uZW50KGNjLkxhYmVsKS5zdHJpbmcgPSBjYy5zeXMubG9jYWxTdG9yYWdlLmdldEl0ZW0oJ2dhbWVfYXZlcmFnZScpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNjLmZpbmQoJ0NhbnZhcy/miJjnu6kv5bmz5Z2H5a2Y5rS75pWwJykuZ2V0Q29tcG9uZW50KGNjLkxhYmVsKS5zdHJpbmcgPSAnMOaciCc7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIOW8gOWFs+eVjOmdoih0YXJnZXQsIGRhdGEpIHtcclxuICAgICAgICBpZiAoZGF0YSA9PSAn5byAJykge1xyXG4gICAgICAgICAgICBjYy5maW5kKCdDYW52YXMv6K6+572u55WM6Z2iJykuYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICAgICAgY2MuZmluZCgnQ2FudmFzL+S4gOe6p+iSmeeJiCcpLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgICAgIGNjLmRpcmVjdG9yLmxvYWRTY2VuZSgncm9hZCcpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNjLmZpbmQoJ0NhbnZhcy/orr7nva7nlYzpnaInKS5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICAgICAgY2MuZmluZCgnQ2FudmFzL+S4gOe6p+iSmeeJiCcpLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICDmtojmga/mj5DnpLoobXNnKSB7XHJcbiAgICAgICAgY2MuYXVkaW9FbmdpbmUucGxheSh0aGlzLm1lZGlhX2Vycm9yLCBmYWxzZSwgYWxsLnZvbHVtZSk7XHJcbiAgICAgICAgY2MuZmluZCgnQ2FudmFzL+a2iOaBr+iDjOaZr+advy/mtojmga8nKS5nZXRDb21wb25lbnQoY2MuTGFiZWwpLnN0cmluZyA9IG1zZztcclxuICAgICAgICBjYy5maW5kKCdDYW52YXMv5raI5oGv6IOM5pmv5p2/JykuZ2V0Q29tcG9uZW50KGNjLkFuaW1hdGlvbikucGxheSgpO1xyXG4gICAgfSxcclxuICAgIOaooeW8j+WKoOWHjyh0YXJnZXQsIGRhdGEpIHtcclxuXHJcbiAgICB9XHJcblxyXG59KTtcclxuIl19