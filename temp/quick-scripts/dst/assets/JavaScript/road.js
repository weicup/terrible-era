
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/JavaScript/road.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'b16cbVUMO9GkLyWEgjZfVii', 'road');
// JavaScript/road.js

"use strict";

// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
cc.Class({
  "extends": cc.Component,
  properties: {},
  onLoad: function onLoad() {
    cc.director.preloadScene('login', this.onProgress, function () {
      cc.find('Canvas/进度条状态').getComponent(cc.Label).string = '加载完成';
      setTimeout(function () {
        cc.director.loadScene('game');
      }, 1000);
    });
  },
  onProgress: function onProgress(completed, total) {
    cc.find('Canvas/进度条/进度条百分比').getComponent(cc.Label).string = Math.floor(completed / total) * 100 + '%';
    cc.find('Canvas/进度条').getComponent(cc.ProgressBar).progress = completed / total;
  }
});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcSmF2YVNjcmlwdFxccm9hZC5qcyJdLCJuYW1lcyI6WyJjYyIsIkNsYXNzIiwiQ29tcG9uZW50IiwicHJvcGVydGllcyIsIm9uTG9hZCIsImRpcmVjdG9yIiwicHJlbG9hZFNjZW5lIiwib25Qcm9ncmVzcyIsImZpbmQiLCJnZXRDb21wb25lbnQiLCJMYWJlbCIsInN0cmluZyIsInNldFRpbWVvdXQiLCJsb2FkU2NlbmUiLCJjb21wbGV0ZWQiLCJ0b3RhbCIsIk1hdGgiLCJmbG9vciIsIlByb2dyZXNzQmFyIiwicHJvZ3Jlc3MiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUFBLEVBQUUsQ0FBQ0MsS0FBSCxDQUFTO0FBQ0wsYUFBU0QsRUFBRSxDQUFDRSxTQURQO0FBR0xDLEVBQUFBLFVBQVUsRUFBRSxFQUhQO0FBS0xDLEVBQUFBLE1BTEssb0JBS0k7QUFDTEosSUFBQUEsRUFBRSxDQUFDSyxRQUFILENBQVlDLFlBQVosQ0FBeUIsT0FBekIsRUFBa0MsS0FBS0MsVUFBdkMsRUFBbUQsWUFBWTtBQUMzRFAsTUFBQUEsRUFBRSxDQUFDUSxJQUFILENBQVEsY0FBUixFQUF3QkMsWUFBeEIsQ0FBcUNULEVBQUUsQ0FBQ1UsS0FBeEMsRUFBK0NDLE1BQS9DLEdBQXdELE1BQXhEO0FBQ0FDLE1BQUFBLFVBQVUsQ0FBQyxZQUFNO0FBQ2JaLFFBQUFBLEVBQUUsQ0FBQ0ssUUFBSCxDQUFZUSxTQUFaLENBQXNCLE1BQXRCO0FBQ0gsT0FGUyxFQUVQLElBRk8sQ0FBVjtBQUdILEtBTEQ7QUFPSCxHQWJJO0FBY0xOLEVBQUFBLFVBZEssc0JBY01PLFNBZE4sRUFjaUJDLEtBZGpCLEVBY3dCO0FBQ3pCZixJQUFBQSxFQUFFLENBQUNRLElBQUgsQ0FBUSxtQkFBUixFQUE2QkMsWUFBN0IsQ0FBMENULEVBQUUsQ0FBQ1UsS0FBN0MsRUFBb0RDLE1BQXBELEdBQTZESyxJQUFJLENBQUNDLEtBQUwsQ0FBV0gsU0FBUyxHQUFHQyxLQUF2QixJQUFnQyxHQUFoQyxHQUFzQyxHQUFuRztBQUNBZixJQUFBQSxFQUFFLENBQUNRLElBQUgsQ0FBUSxZQUFSLEVBQXNCQyxZQUF0QixDQUFtQ1QsRUFBRSxDQUFDa0IsV0FBdEMsRUFBbURDLFFBQW5ELEdBQThETCxTQUFTLEdBQUdDLEtBQTFFO0FBQ0g7QUFqQkksQ0FBVCIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8gTGVhcm4gY2MuQ2xhc3M6XHJcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2NsYXNzLmh0bWxcclxuLy8gTGVhcm4gQXR0cmlidXRlOlxyXG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXHJcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxyXG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXHJcblxyXG5jYy5DbGFzcyh7XHJcbiAgICBleHRlbmRzOiBjYy5Db21wb25lbnQsXHJcblxyXG4gICAgcHJvcGVydGllczoge1xyXG4gICAgfSxcclxuICAgIG9uTG9hZCgpIHtcclxuICAgICAgICBjYy5kaXJlY3Rvci5wcmVsb2FkU2NlbmUoJ2xvZ2luJywgdGhpcy5vblByb2dyZXNzLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIGNjLmZpbmQoJ0NhbnZhcy/ov5vluqbmnaHnirbmgIEnKS5nZXRDb21wb25lbnQoY2MuTGFiZWwpLnN0cmluZyA9ICfliqDovb3lrozmiJAnO1xyXG4gICAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICAgICAgICAgIGNjLmRpcmVjdG9yLmxvYWRTY2VuZSgnZ2FtZScpO1xyXG4gICAgICAgICAgICB9LCAxMDAwKTtcclxuICAgICAgICB9KVxyXG5cclxuICAgIH0sXHJcbiAgICBvblByb2dyZXNzKGNvbXBsZXRlZCwgdG90YWwpIHtcclxuICAgICAgICBjYy5maW5kKCdDYW52YXMv6L+b5bqm5p2hL+i/m+W6puadoeeZvuWIhuavlCcpLmdldENvbXBvbmVudChjYy5MYWJlbCkuc3RyaW5nID0gTWF0aC5mbG9vcihjb21wbGV0ZWQgLyB0b3RhbCkgKiAxMDAgKyAnJSc7XHJcbiAgICAgICAgY2MuZmluZCgnQ2FudmFzL+i/m+W6puadoScpLmdldENvbXBvbmVudChjYy5Qcm9ncmVzc0JhcikucHJvZ3Jlc3MgPSBjb21wbGV0ZWQgLyB0b3RhbDtcclxuICAgIH1cclxuXHJcbn0pO1xyXG4iXX0=