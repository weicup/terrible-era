
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/JavaScript/全局变量.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'fe3d5YyKTVMP5nxiUMo2G90', '全局变量');
// JavaScript/全局变量.js

"use strict";

module.exports = {
  account: '',
  name: '',
  房间准备: false,
  玩家组: [],
  人物组: [],
  初始人物组: [],
  人物拥有: 0,
  选中: -1,
  时间: 1,
  提示框: false,
  food: [0, 10],
  mood: [0, 5],
  stone: [0, 5],
  //====================
  volume: 1,
  //背景音乐音量
  prerole: [],
  //预选人物列表
  selectNum: 0,
  //人物选择数量
  role: [],
  //人物列表
  姓: ['慕容', '李', '张', '刘', '诸葛', '陈', '杨', '龙', '马', '王', '孟', '乌', '井', '裴', '陆', '左', '程', '贾', '崔', '赵', '周', '吴', '郑', '沈', '蒋', '韩', '关', '朱', '凌', '霍', '公孙', '司空', '司徒', '上官', '萧'],
  男名: ['玮', '帝', '洪', '逸星', '涛', '杰伦', '霆锋', '赫', '国国', '羽', '备', '飞', '子龙', '嘉林', '峰', '三丰', '傲天', '于晏', '勇', '权', '坚', '亮', '刚', '子文', '子若', '子墨', '墨', '恒', '家博', '弘', '辉', '东', '西', '南', '北', '杰', '俊杰', '冉', '以太', '帅'],
  女名: ['雯雯', '诺', '诗雨', '欣茹', '美娟', '琳', '微', '薇', '子涵', '菲', '菲菲', '嘉怡', '燕', '妙曼', '蔓蔓', '玉', '桂花', '梓蓉', '珂', '安怡', '慧馨', '慧昕', '慧欣', '花花', '晓丽', '梓彤', '晓欣', '亦舒', '紫萱', '亚轩', '霏霏', '妍文', '子涵', '幂', '芮'],
  优点: ['小鸟胃', '后起之秀', '好学', '完美', '幸运儿', '强壮'],
  缺点: ['大胃王', '娇弱', '愚钝', '倒霉蛋', '捣蛋鬼', '小偷'],
  main: {
    time: 0,
    month: 0,
    year: 0,
    season: '',
    凶吉: '平安无事'
  }
};

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcSmF2YVNjcmlwdFxc5YWo5bGA5Y+Y6YePLmpzIl0sIm5hbWVzIjpbIm1vZHVsZSIsImV4cG9ydHMiLCJhY2NvdW50IiwibmFtZSIsIuaIv+mXtOWHhuWkhyIsIueOqeWutue7hCIsIuS6uueJqee7hCIsIuWIneWni+S6uueJqee7hCIsIuS6uueJqeaLpeaciSIsIumAieS4rSIsIuaXtumXtCIsIuaPkOekuuahhiIsImZvb2QiLCJtb29kIiwic3RvbmUiLCJ2b2x1bWUiLCJwcmVyb2xlIiwic2VsZWN0TnVtIiwicm9sZSIsIuWnkyIsIueUt+WQjSIsIuWls+WQjSIsIuS8mOeCuSIsIue8uueCuSIsIm1haW4iLCJ0aW1lIiwibW9udGgiLCJ5ZWFyIiwic2Vhc29uIiwi5Ye25ZCJIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUFBQSxNQUFNLENBQUNDLE9BQVAsR0FBaUI7QUFDYkMsRUFBQUEsT0FBTyxFQUFFLEVBREk7QUFFYkMsRUFBQUEsSUFBSSxFQUFFLEVBRk87QUFHYkMsRUFBQUEsSUFBSSxFQUFFLEtBSE87QUFJYkMsRUFBQUEsR0FBRyxFQUFFLEVBSlE7QUFLYkMsRUFBQUEsR0FBRyxFQUFFLEVBTFE7QUFNYkMsRUFBQUEsS0FBSyxFQUFFLEVBTk07QUFPYkMsRUFBQUEsSUFBSSxFQUFFLENBUE87QUFRYkMsRUFBQUEsRUFBRSxFQUFFLENBQUMsQ0FSUTtBQVNiQyxFQUFBQSxFQUFFLEVBQUUsQ0FUUztBQVViQyxFQUFBQSxHQUFHLEVBQUUsS0FWUTtBQVdiQyxFQUFBQSxJQUFJLEVBQUUsQ0FBQyxDQUFELEVBQUksRUFBSixDQVhPO0FBWWJDLEVBQUFBLElBQUksRUFBRSxDQUFDLENBQUQsRUFBSSxDQUFKLENBWk87QUFhYkMsRUFBQUEsS0FBSyxFQUFFLENBQUMsQ0FBRCxFQUFJLENBQUosQ0FiTTtBQWdCYjtBQUNBQyxFQUFBQSxNQUFNLEVBQUUsQ0FqQks7QUFpQkQ7QUFDWkMsRUFBQUEsT0FBTyxFQUFFLEVBbEJJO0FBa0JDO0FBQ2RDLEVBQUFBLFNBQVMsRUFBRSxDQW5CRTtBQW1CQztBQUNkQyxFQUFBQSxJQUFJLEVBQUUsRUFwQk87QUFvQkE7QUFDYkMsRUFBQUEsQ0FBQyxFQUFFLENBQUMsSUFBRCxFQUFPLEdBQVAsRUFBWSxHQUFaLEVBQWlCLEdBQWpCLEVBQXNCLElBQXRCLEVBQTRCLEdBQTVCLEVBQWlDLEdBQWpDLEVBQXNDLEdBQXRDLEVBQTJDLEdBQTNDLEVBQWdELEdBQWhELEVBQXFELEdBQXJELEVBQTBELEdBQTFELEVBQStELEdBQS9ELEVBQW9FLEdBQXBFLEVBQXlFLEdBQXpFLEVBQThFLEdBQTlFLEVBQW1GLEdBQW5GLEVBQXdGLEdBQXhGLEVBQTZGLEdBQTdGLEVBQWtHLEdBQWxHLEVBQXVHLEdBQXZHLEVBQTRHLEdBQTVHLEVBQWlILEdBQWpILEVBQXNILEdBQXRILEVBQTJILEdBQTNILEVBQWdJLEdBQWhJLEVBQXFJLEdBQXJJLEVBQTBJLEdBQTFJLEVBQStJLEdBQS9JLEVBQW9KLEdBQXBKLEVBQXlKLElBQXpKLEVBQStKLElBQS9KLEVBQXFLLElBQXJLLEVBQTJLLElBQTNLLEVBQWlMLEdBQWpMLENBckJVO0FBc0JiQyxFQUFBQSxFQUFFLEVBQUUsQ0FBQyxHQUFELEVBQU0sR0FBTixFQUFXLEdBQVgsRUFBZ0IsSUFBaEIsRUFBc0IsR0FBdEIsRUFBMkIsSUFBM0IsRUFBaUMsSUFBakMsRUFBdUMsR0FBdkMsRUFBNEMsSUFBNUMsRUFBa0QsR0FBbEQsRUFBdUQsR0FBdkQsRUFBNEQsR0FBNUQsRUFBaUUsSUFBakUsRUFBdUUsSUFBdkUsRUFBNkUsR0FBN0UsRUFBa0YsSUFBbEYsRUFBd0YsSUFBeEYsRUFBOEYsSUFBOUYsRUFBb0csR0FBcEcsRUFBeUcsR0FBekcsRUFBOEcsR0FBOUcsRUFBbUgsR0FBbkgsRUFBd0gsR0FBeEgsRUFBNkgsSUFBN0gsRUFBbUksSUFBbkksRUFBeUksSUFBekksRUFBK0ksR0FBL0ksRUFBb0osR0FBcEosRUFBeUosSUFBekosRUFBK0osR0FBL0osRUFBb0ssR0FBcEssRUFBeUssR0FBekssRUFBOEssR0FBOUssRUFBbUwsR0FBbkwsRUFBd0wsR0FBeEwsRUFBNkwsR0FBN0wsRUFBa00sSUFBbE0sRUFBd00sR0FBeE0sRUFBNk0sSUFBN00sRUFBbU4sR0FBbk4sQ0F0QlM7QUF1QmJDLEVBQUFBLEVBQUUsRUFBRSxDQUFDLElBQUQsRUFBTyxHQUFQLEVBQVksSUFBWixFQUFrQixJQUFsQixFQUF3QixJQUF4QixFQUE4QixHQUE5QixFQUFtQyxHQUFuQyxFQUF3QyxHQUF4QyxFQUE2QyxJQUE3QyxFQUFtRCxHQUFuRCxFQUF3RCxJQUF4RCxFQUE4RCxJQUE5RCxFQUFvRSxHQUFwRSxFQUF5RSxJQUF6RSxFQUErRSxJQUEvRSxFQUFxRixHQUFyRixFQUEwRixJQUExRixFQUFnRyxJQUFoRyxFQUFzRyxHQUF0RyxFQUEyRyxJQUEzRyxFQUFpSCxJQUFqSCxFQUF1SCxJQUF2SCxFQUE2SCxJQUE3SCxFQUFtSSxJQUFuSSxFQUF5SSxJQUF6SSxFQUErSSxJQUEvSSxFQUFxSixJQUFySixFQUEySixJQUEzSixFQUFpSyxJQUFqSyxFQUF1SyxJQUF2SyxFQUE2SyxJQUE3SyxFQUFtTCxJQUFuTCxFQUF5TCxJQUF6TCxFQUErTCxHQUEvTCxFQUFvTSxHQUFwTSxDQXZCUztBQXdCYkMsRUFBQUEsRUFBRSxFQUFFLENBQUMsS0FBRCxFQUFRLE1BQVIsRUFBZ0IsSUFBaEIsRUFBc0IsSUFBdEIsRUFBNEIsS0FBNUIsRUFBbUMsSUFBbkMsQ0F4QlM7QUF5QmJDLEVBQUFBLEVBQUUsRUFBRSxDQUFDLEtBQUQsRUFBUSxJQUFSLEVBQWMsSUFBZCxFQUFvQixLQUFwQixFQUEyQixLQUEzQixFQUFrQyxJQUFsQyxDQXpCUztBQTBCYkMsRUFBQUEsSUFBSSxFQUFFO0FBQ0ZDLElBQUFBLElBQUksRUFBRSxDQURKO0FBRUZDLElBQUFBLEtBQUssRUFBRSxDQUZMO0FBR0ZDLElBQUFBLElBQUksRUFBRSxDQUhKO0FBSUZDLElBQUFBLE1BQU0sRUFBRSxFQUpOO0FBS0ZDLElBQUFBLEVBQUUsRUFBRTtBQUxGO0FBMUJPLENBQWpCIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJtb2R1bGUuZXhwb3J0cyA9IHtcclxuICAgIGFjY291bnQ6ICcnLFxyXG4gICAgbmFtZTogJycsXHJcbiAgICDmiL/pl7Tlh4blpIc6IGZhbHNlLFxyXG4gICAg546p5a6257uEOiBbXSxcclxuICAgIOS6uueJqee7hDogW10sXHJcbiAgICDliJ3lp4vkurrniannu4Q6IFtdLFxyXG4gICAg5Lq654mp5oul5pyJOiAwLFxyXG4gICAg6YCJ5LitOiAtMSxcclxuICAgIOaXtumXtDogMSxcclxuICAgIOaPkOekuuahhjogZmFsc2UsXHJcbiAgICBmb29kOiBbMCwgMTBdLFxyXG4gICAgbW9vZDogWzAsIDVdLFxyXG4gICAgc3RvbmU6IFswLCA1XSxcclxuXHJcblxyXG4gICAgLy89PT09PT09PT09PT09PT09PT09PVxyXG4gICAgdm9sdW1lOiAxLCAgLy/og4zmma/pn7PkuZDpn7Pph49cclxuICAgIHByZXJvbGU6IFtdLCAgLy/pooTpgInkurrnianliJfooahcclxuICAgIHNlbGVjdE51bTogMCwgLy/kurrnianpgInmi6nmlbDph49cclxuICAgIHJvbGU6IFtdLCAgICAvL+S6uueJqeWIl+ihqFxyXG4gICAg5aeTOiBbJ+aFleWuuScsICfmnY4nLCAn5bygJywgJ+WImCcsICfor7jokZsnLCAn6ZmIJywgJ+adqCcsICfpvpknLCAn6amsJywgJ+eOiycsICflrZ8nLCAn5LmMJywgJ+S6lScsICfoo7QnLCAn6ZmGJywgJ+W3picsICfnqIsnLCAn6LS+JywgJ+W0lCcsICfotbUnLCAn5ZGoJywgJ+WQtCcsICfpg5EnLCAn5rKIJywgJ+iSiycsICfpn6knLCAn5YWzJywgJ+acsScsICflh4wnLCAn6ZyNJywgJ+WFrOWtmScsICflj7jnqbonLCAn5Y+45b6SJywgJ+S4iuWumCcsICfokKcnXSxcclxuICAgIOeUt+WQjTogWyfnjq4nLCAn5bidJywgJ+a0qicsICfpgLjmmJ8nLCAn5rabJywgJ+adsOS8picsICfpnIbplIsnLCAn6LWrJywgJ+WbveWbvScsICfnvr0nLCAn5aSHJywgJ+mjnicsICflrZDpvpknLCAn5ZiJ5p6XJywgJ+WzsCcsICfkuInkuLAnLCAn5YKy5aSpJywgJ+S6juaZjycsICfli4cnLCAn5p2DJywgJ+WdmicsICfkuq4nLCAn5YiaJywgJ+WtkOaWhycsICflrZDoi6UnLCAn5a2Q5aKoJywgJ+WiqCcsICfmgZInLCAn5a625Y2aJywgJ+W8mCcsICfovoknLCAn5LicJywgJ+ilvycsICfljZcnLCAn5YyXJywgJ+adsCcsICfkv4rmnbAnLCAn5YaJJywgJ+S7peWkqicsICfluIUnXSxcclxuICAgIOWls+WQjTogWyfpm6/pm68nLCAn6K+6JywgJ+ivl+mbqCcsICfmrKPojLknLCAn576O5aifJywgJ+eQsycsICflvq4nLCAn6JaHJywgJ+WtkOa2tScsICfoj7InLCAn6I+y6I+yJywgJ+WYieaAoScsICfnh5UnLCAn5aaZ5pu8JywgJ+iUk+iUkycsICfnjoknLCAn5qGC6IqxJywgJ+aik+iTiScsICfnj4InLCAn5a6J5oChJywgJ+aFp+mmqCcsICfmhafmmJUnLCAn5oWn5qyjJywgJ+iKseiKsScsICfmmZPkuL0nLCAn5qKT5b2kJywgJ+aZk+asoycsICfkuqboiJInLCAn57Sr6JCxJywgJ+S6mui9qScsICfpnI/pnI8nLCAn5aaN5paHJywgJ+WtkOa2tScsICfluYInLCAn6IquJ10sXHJcbiAgICDkvJjngrk6IFsn5bCP6bif6IODJywgJ+WQjui1t+S5i+engCcsICflpb3lraYnLCAn5a6M576OJywgJ+W5uOi/kOWEvycsICflvLrlo64nXSxcclxuICAgIOe8uueCuTogWyflpKfog4PnjosnLCAn5aiH5byxJywgJ+aEmumSnScsICflgJLpnInom4snLCAn5o2j6JuL6ay8JywgJ+Wwj+WBtyddLFxyXG4gICAgbWFpbjoge1xyXG4gICAgICAgIHRpbWU6IDAsXHJcbiAgICAgICAgbW9udGg6IDAsXHJcbiAgICAgICAgeWVhcjogMCxcclxuICAgICAgICBzZWFzb246ICcnLFxyXG4gICAgICAgIOWHtuWQiTogJ+W5s+WuieaXoOS6iycsXHJcbiAgICB9XHJcbn07XHJcbiJdfQ==