
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/JavaScript/game/日程更新.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '64ebfOncv1FaJxbVsA64J+a', '日程更新');
// JavaScript/game/日程更新.js

"use strict";

// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
cc.Class({
  "extends": cc.Component,
  properties: {},
  日程更新: function _() {
    cc.find('Canvas/黑屏').getComponent(cc.Animation).play('黑屏');
    setTimeout(function () {
      cc.find('Canvas/黑屏/时间').getComponent(cc.Label).string = Math.floor(self.时间 / 4) + '年' + self.时间 % 4 + '月';
      cc.find('Canvas/黑屏/时间').active = true;
      cc.find('Canvas/初始界面').active = false; //人物组处理
      //清空原有人物组

      cc.find('Canvas/下界面/人物池/view/content').destroyAllChildren();
      cc.find('Canvas/中左界面/view/content').destroyAllChildren(); //生成新人物组

      for (var i = 0; i < self.人物组.length; i++) {
        var node = cc.instantiate(cc.find('game').getComponent('game').人物);

        if (self.人物组[i].性别 == '男') {
          node.getChildByName('头像').getComponent(cc.Sprite).spriteFrame = cc.find('game').getComponent('game').男.getSpriteFrame(self.人物组[i].头像);
        } else {
          node.getChildByName('头像').getComponent(cc.Sprite).spriteFrame = cc.find('game').getComponent('game').女.getSpriteFrame(self.人物组[i].头像);
        }

        node.getChildByName('头像').getChildByName('名字').getComponent(cc.Label).string = self.人物组[i].名字;
        node.getChildByName('工作').getComponent(cc.Label).string = self.人物组[i].工作;
        node.getChildByName('控制权').getComponent(cc.Label).string = self.人物组[i].控制权;
        node.name = i + '';

        if (self.人物组[i].控制权 == self.name) {
          node.parent = cc.find('Canvas/下界面/人物池/view/content');
          node.getComponent(cc.Button).clickEvents[0].customEventData = '自己';
        } else {
          node.parent = cc.find('Canvas/中左界面/view/content');
          node.getComponent('人物简介').注册热键();
        }

        cc.find('Canvas/下界面/人物状态/信息/view/content').active = false;
      }

      cc.find('Canvas/黑屏/时间').active = false;
      cc.find('Canvas/黑屏').getComponent(cc.Animation).play('白屏');
      cc.find('Canvas/日程详情/工作/view/content').destroyAllChildren(); // 日程详情添加人物工作

      setTimeout(function () {
        for (var i = 0; i < self.人物组.length; i++) {
          var node = cc.instantiate(cc.find('game').getComponent('game').日程详情_工作);

          if (self.人物组[i].工作 != '休息') {
            //复制节点进日程详情
            if (self.人物组[i].性别 == '男') {
              node.getChildByName('头像').getComponent(cc.Sprite).spriteFrame = cc.find('game').getComponent('game').男.getSpriteFrame(self.人物组[i].头像);
            } else {
              node.getChildByName('头像').getComponent(cc.Sprite).spriteFrame = cc.find('game').getComponent('game').女.getSpriteFrame(self.人物组[i].头像);
            }

            node.getChildByName('名字').getComponent(cc.Label).string = self.人物组[i].名字 + '(' + self.人物组[i].控制权 + ')';
            node.getChildByName('工作').getComponent(cc.Label).string = self.人物组[i].工作;
            node.getChildByName('收获量').getComponent(cc.Label).string = self.人物组[i].收获量; //食物采集计算

            if (self.人物组[i].工作 == '采集' || self.人物组[i].工作 == '狩猎') {
              self.food[0] = self.food[0] * 1 + self.人物组[i].收获量 * 1;
              cc.find('Canvas/上界面/区域界面/资源/食物/2').getComponent(cc.Label).string = self.food[0] + '/' + self.food[1];
            } //木头采集计算
            else if (self.人物组[i].工作 == '砍伐') {
                self.mood[0] = self.mood[0] * 1 + self.人物组[i].收获量 * 1;

                if (self.mood[0] >= self.mood[1]) {
                  self.mood[0] = self.mood[1];
                }

                cc.find('Canvas/上界面/区域界面/资源/木头/2').getComponent(cc.Label).string = self.mood[0] + '/' + self.mood[1];
              } //石头采集计算
              else if (self.人物组[i].工作 == '挖掘') {
                  self.stone[0] = self.stone[0] * 1 + self.人物组[i].收获量 * 1;

                  if (self.stone[0] >= self.stone[1]) {
                    self.stone[0] = self.stone[1];
                  }

                  cc.find('Canvas/上界面/区域界面/资源/石头/2').getComponent(cc.Label).string = self.stone[0] + '/' + self.stone[1];
                } //经验值计算


            node.getChildByName('经验值').getComponent(cc.Label).string = self.人物组[i].收获经验值; //绑定父节点

            node.parent = cc.find('Canvas/日程详情/工作/view/content');
          }
        } //食物消耗计算


        self.food[0] = (self.food[0] * 1 - msg.进食量 * 1).toFixed(2);

        if (self.food[0] <= 0) {
          console.log('游戏结束');
        }

        if (self.food[0] >= self.food[1]) {
          self.food[0] = Math.floor(self.food[1]);
        } //食物更新


        cc.find('Canvas/上界面/区域界面/资源/食物/2').getComponent(cc.Label).string = self.food[0] + '/' + self.food[1]; //打开日程详情界面

        cc.find('Canvas').pauseSystemEvents();
        cc.find('Canvas/二级遮罩').active = true;
        cc.find('Canvas/日程详情').active = true; //准备状态重置

        cc.find('Canvas/中右界面/准备').getComponent(cc.Button).interactable = true;
        cc.find('Canvas/中右界面/准备').getComponent(cc.Button).clickEvents[1].customEventData = '准备';
        cc.find('Canvas/中右界面/准备').getComponent(cc.Sprite).spriteFrame = cc.find('game').getComponent('game').准备状态.getSpriteFrame('准备');
        cc.find('Canvas/下界面/人物池/view/选中框').active = false;
        self.选中 = -1;
        cc.find('Canvas/日程详情').getComponent(cc.Animation).play();
        cc.find('Canvas').resumeSystemEvents();
      }, 3000);
    }, 3000);
  }
});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcSmF2YVNjcmlwdFxcZ2FtZVxc5pel56iL5pu05pawLmpzIl0sIm5hbWVzIjpbImNjIiwiQ2xhc3MiLCJDb21wb25lbnQiLCJwcm9wZXJ0aWVzIiwi5pel56iL5pu05pawIiwiZmluZCIsImdldENvbXBvbmVudCIsIkFuaW1hdGlvbiIsInBsYXkiLCJzZXRUaW1lb3V0IiwiTGFiZWwiLCJzdHJpbmciLCJNYXRoIiwiZmxvb3IiLCJzZWxmIiwi5pe26Ze0IiwiYWN0aXZlIiwiZGVzdHJveUFsbENoaWxkcmVuIiwiaSIsIuS6uueJqee7hCIsImxlbmd0aCIsIm5vZGUiLCJpbnN0YW50aWF0ZSIsIuS6uueJqSIsIuaAp+WIqyIsImdldENoaWxkQnlOYW1lIiwiU3ByaXRlIiwic3ByaXRlRnJhbWUiLCLnlLciLCJnZXRTcHJpdGVGcmFtZSIsIuWktOWDjyIsIuWlsyIsIuWQjeWtlyIsIuW3peS9nCIsIuaOp+WItuadgyIsIm5hbWUiLCJwYXJlbnQiLCJCdXR0b24iLCJjbGlja0V2ZW50cyIsImN1c3RvbUV2ZW50RGF0YSIsIuazqOWGjOeDremUriIsIuaXpeeoi+ivpuaDhV/lt6XkvZwiLCLmlLbojrfph48iLCJmb29kIiwibW9vZCIsInN0b25lIiwi5pS26I6357uP6aqM5YC8IiwibXNnIiwi6L+b6aOf6YePIiwidG9GaXhlZCIsImNvbnNvbGUiLCJsb2ciLCJwYXVzZVN5c3RlbUV2ZW50cyIsImludGVyYWN0YWJsZSIsIuWHhuWkh+eKtuaAgSIsIumAieS4rSIsInJlc3VtZVN5c3RlbUV2ZW50cyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQUEsRUFBRSxDQUFDQyxLQUFILENBQVM7QUFDTCxhQUFTRCxFQUFFLENBQUNFLFNBRFA7QUFHTEMsRUFBQUEsVUFBVSxFQUFFLEVBSFA7QUFPTEMsRUFBQUEsSUFQSyxlQU9FO0FBQ0hKLElBQUFBLEVBQUUsQ0FBQ0ssSUFBSCxDQUFRLFdBQVIsRUFBcUJDLFlBQXJCLENBQWtDTixFQUFFLENBQUNPLFNBQXJDLEVBQWdEQyxJQUFoRCxDQUFxRCxJQUFyRDtBQUNBQyxJQUFBQSxVQUFVLENBQUMsWUFBTTtBQUNiVCxNQUFBQSxFQUFFLENBQUNLLElBQUgsQ0FBUSxjQUFSLEVBQXdCQyxZQUF4QixDQUFxQ04sRUFBRSxDQUFDVSxLQUF4QyxFQUErQ0MsTUFBL0MsR0FBd0RDLElBQUksQ0FBQ0MsS0FBTCxDQUFXQyxJQUFJLENBQUNDLEVBQUwsR0FBVSxDQUFyQixJQUEwQixHQUExQixHQUFnQ0QsSUFBSSxDQUFDQyxFQUFMLEdBQVUsQ0FBMUMsR0FBOEMsR0FBdEc7QUFDQWYsTUFBQUEsRUFBRSxDQUFDSyxJQUFILENBQVEsY0FBUixFQUF3QlcsTUFBeEIsR0FBaUMsSUFBakM7QUFDQWhCLE1BQUFBLEVBQUUsQ0FBQ0ssSUFBSCxDQUFRLGFBQVIsRUFBdUJXLE1BQXZCLEdBQWdDLEtBQWhDLENBSGEsQ0FJYjtBQUNBOztBQUNBaEIsTUFBQUEsRUFBRSxDQUFDSyxJQUFILENBQVEsNkJBQVIsRUFBdUNZLGtCQUF2QztBQUNBakIsTUFBQUEsRUFBRSxDQUFDSyxJQUFILENBQVEsMEJBQVIsRUFBb0NZLGtCQUFwQyxHQVBhLENBU2I7O0FBQ0EsV0FBSyxJQUFJQyxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHSixJQUFJLENBQUNLLEdBQUwsQ0FBU0MsTUFBN0IsRUFBcUNGLENBQUMsRUFBdEMsRUFBMEM7QUFDdEMsWUFBSUcsSUFBSSxHQUFHckIsRUFBRSxDQUFDc0IsV0FBSCxDQUFldEIsRUFBRSxDQUFDSyxJQUFILENBQVEsTUFBUixFQUFnQkMsWUFBaEIsQ0FBNkIsTUFBN0IsRUFBcUNpQixFQUFwRCxDQUFYOztBQUNBLFlBQUlULElBQUksQ0FBQ0ssR0FBTCxDQUFTRCxDQUFULEVBQVlNLEVBQVosSUFBa0IsR0FBdEIsRUFBMkI7QUFDdkJILFVBQUFBLElBQUksQ0FBQ0ksY0FBTCxDQUFvQixJQUFwQixFQUEwQm5CLFlBQTFCLENBQXVDTixFQUFFLENBQUMwQixNQUExQyxFQUFrREMsV0FBbEQsR0FBZ0UzQixFQUFFLENBQUNLLElBQUgsQ0FBUSxNQUFSLEVBQWdCQyxZQUFoQixDQUE2QixNQUE3QixFQUFxQ3NCLENBQXJDLENBQXVDQyxjQUF2QyxDQUFzRGYsSUFBSSxDQUFDSyxHQUFMLENBQVNELENBQVQsRUFBWVksRUFBbEUsQ0FBaEU7QUFDSCxTQUZELE1BRU87QUFDSFQsVUFBQUEsSUFBSSxDQUFDSSxjQUFMLENBQW9CLElBQXBCLEVBQTBCbkIsWUFBMUIsQ0FBdUNOLEVBQUUsQ0FBQzBCLE1BQTFDLEVBQWtEQyxXQUFsRCxHQUFnRTNCLEVBQUUsQ0FBQ0ssSUFBSCxDQUFRLE1BQVIsRUFBZ0JDLFlBQWhCLENBQTZCLE1BQTdCLEVBQXFDeUIsQ0FBckMsQ0FBdUNGLGNBQXZDLENBQXNEZixJQUFJLENBQUNLLEdBQUwsQ0FBU0QsQ0FBVCxFQUFZWSxFQUFsRSxDQUFoRTtBQUNIOztBQUNEVCxRQUFBQSxJQUFJLENBQUNJLGNBQUwsQ0FBb0IsSUFBcEIsRUFBMEJBLGNBQTFCLENBQXlDLElBQXpDLEVBQStDbkIsWUFBL0MsQ0FBNEROLEVBQUUsQ0FBQ1UsS0FBL0QsRUFBc0VDLE1BQXRFLEdBQStFRyxJQUFJLENBQUNLLEdBQUwsQ0FBU0QsQ0FBVCxFQUFZYyxFQUEzRjtBQUNBWCxRQUFBQSxJQUFJLENBQUNJLGNBQUwsQ0FBb0IsSUFBcEIsRUFBMEJuQixZQUExQixDQUF1Q04sRUFBRSxDQUFDVSxLQUExQyxFQUFpREMsTUFBakQsR0FBMERHLElBQUksQ0FBQ0ssR0FBTCxDQUFTRCxDQUFULEVBQVllLEVBQXRFO0FBQ0FaLFFBQUFBLElBQUksQ0FBQ0ksY0FBTCxDQUFvQixLQUFwQixFQUEyQm5CLFlBQTNCLENBQXdDTixFQUFFLENBQUNVLEtBQTNDLEVBQWtEQyxNQUFsRCxHQUEyREcsSUFBSSxDQUFDSyxHQUFMLENBQVNELENBQVQsRUFBWWdCLEdBQXZFO0FBQ0FiLFFBQUFBLElBQUksQ0FBQ2MsSUFBTCxHQUFZakIsQ0FBQyxHQUFHLEVBQWhCOztBQUNBLFlBQUlKLElBQUksQ0FBQ0ssR0FBTCxDQUFTRCxDQUFULEVBQVlnQixHQUFaLElBQW1CcEIsSUFBSSxDQUFDcUIsSUFBNUIsRUFBa0M7QUFDOUJkLFVBQUFBLElBQUksQ0FBQ2UsTUFBTCxHQUFjcEMsRUFBRSxDQUFDSyxJQUFILENBQVEsNkJBQVIsQ0FBZDtBQUNBZ0IsVUFBQUEsSUFBSSxDQUFDZixZQUFMLENBQWtCTixFQUFFLENBQUNxQyxNQUFyQixFQUE2QkMsV0FBN0IsQ0FBeUMsQ0FBekMsRUFBNENDLGVBQTVDLEdBQThELElBQTlEO0FBQ0gsU0FIRCxNQUdPO0FBQ0hsQixVQUFBQSxJQUFJLENBQUNlLE1BQUwsR0FBY3BDLEVBQUUsQ0FBQ0ssSUFBSCxDQUFRLDBCQUFSLENBQWQ7QUFDQWdCLFVBQUFBLElBQUksQ0FBQ2YsWUFBTCxDQUFrQixNQUFsQixFQUEwQmtDLElBQTFCO0FBQ0g7O0FBQ0R4QyxRQUFBQSxFQUFFLENBQUNLLElBQUgsQ0FBUSxpQ0FBUixFQUEyQ1csTUFBM0MsR0FBb0QsS0FBcEQ7QUFDSDs7QUFDRGhCLE1BQUFBLEVBQUUsQ0FBQ0ssSUFBSCxDQUFRLGNBQVIsRUFBd0JXLE1BQXhCLEdBQWlDLEtBQWpDO0FBQ0FoQixNQUFBQSxFQUFFLENBQUNLLElBQUgsQ0FBUSxXQUFSLEVBQXFCQyxZQUFyQixDQUFrQ04sRUFBRSxDQUFDTyxTQUFyQyxFQUFnREMsSUFBaEQsQ0FBcUQsSUFBckQ7QUFDQVIsTUFBQUEsRUFBRSxDQUFDSyxJQUFILENBQVEsNkJBQVIsRUFBdUNZLGtCQUF2QyxHQWhDYSxDQWtDYjs7QUFDQVIsTUFBQUEsVUFBVSxDQUFDLFlBQU07QUFDYixhQUFLLElBQUlTLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUdKLElBQUksQ0FBQ0ssR0FBTCxDQUFTQyxNQUE3QixFQUFxQ0YsQ0FBQyxFQUF0QyxFQUEwQztBQUN0QyxjQUFJRyxJQUFJLEdBQUdyQixFQUFFLENBQUNzQixXQUFILENBQWV0QixFQUFFLENBQUNLLElBQUgsQ0FBUSxNQUFSLEVBQWdCQyxZQUFoQixDQUE2QixNQUE3QixFQUFxQ21DLE9BQXBELENBQVg7O0FBQ0EsY0FBSTNCLElBQUksQ0FBQ0ssR0FBTCxDQUFTRCxDQUFULEVBQVllLEVBQVosSUFBa0IsSUFBdEIsRUFBNEI7QUFDeEI7QUFDQSxnQkFBSW5CLElBQUksQ0FBQ0ssR0FBTCxDQUFTRCxDQUFULEVBQVlNLEVBQVosSUFBa0IsR0FBdEIsRUFBMkI7QUFDdkJILGNBQUFBLElBQUksQ0FBQ0ksY0FBTCxDQUFvQixJQUFwQixFQUEwQm5CLFlBQTFCLENBQXVDTixFQUFFLENBQUMwQixNQUExQyxFQUFrREMsV0FBbEQsR0FBZ0UzQixFQUFFLENBQUNLLElBQUgsQ0FBUSxNQUFSLEVBQWdCQyxZQUFoQixDQUE2QixNQUE3QixFQUFxQ3NCLENBQXJDLENBQXVDQyxjQUF2QyxDQUFzRGYsSUFBSSxDQUFDSyxHQUFMLENBQVNELENBQVQsRUFBWVksRUFBbEUsQ0FBaEU7QUFDSCxhQUZELE1BRU87QUFDSFQsY0FBQUEsSUFBSSxDQUFDSSxjQUFMLENBQW9CLElBQXBCLEVBQTBCbkIsWUFBMUIsQ0FBdUNOLEVBQUUsQ0FBQzBCLE1BQTFDLEVBQWtEQyxXQUFsRCxHQUFnRTNCLEVBQUUsQ0FBQ0ssSUFBSCxDQUFRLE1BQVIsRUFBZ0JDLFlBQWhCLENBQTZCLE1BQTdCLEVBQXFDeUIsQ0FBckMsQ0FBdUNGLGNBQXZDLENBQXNEZixJQUFJLENBQUNLLEdBQUwsQ0FBU0QsQ0FBVCxFQUFZWSxFQUFsRSxDQUFoRTtBQUNIOztBQUNEVCxZQUFBQSxJQUFJLENBQUNJLGNBQUwsQ0FBb0IsSUFBcEIsRUFBMEJuQixZQUExQixDQUF1Q04sRUFBRSxDQUFDVSxLQUExQyxFQUFpREMsTUFBakQsR0FBMERHLElBQUksQ0FBQ0ssR0FBTCxDQUFTRCxDQUFULEVBQVljLEVBQVosR0FBaUIsR0FBakIsR0FBdUJsQixJQUFJLENBQUNLLEdBQUwsQ0FBU0QsQ0FBVCxFQUFZZ0IsR0FBbkMsR0FBeUMsR0FBbkc7QUFDQWIsWUFBQUEsSUFBSSxDQUFDSSxjQUFMLENBQW9CLElBQXBCLEVBQTBCbkIsWUFBMUIsQ0FBdUNOLEVBQUUsQ0FBQ1UsS0FBMUMsRUFBaURDLE1BQWpELEdBQTBERyxJQUFJLENBQUNLLEdBQUwsQ0FBU0QsQ0FBVCxFQUFZZSxFQUF0RTtBQUNBWixZQUFBQSxJQUFJLENBQUNJLGNBQUwsQ0FBb0IsS0FBcEIsRUFBMkJuQixZQUEzQixDQUF3Q04sRUFBRSxDQUFDVSxLQUEzQyxFQUFrREMsTUFBbEQsR0FBMkRHLElBQUksQ0FBQ0ssR0FBTCxDQUFTRCxDQUFULEVBQVl3QixHQUF2RSxDQVR3QixDQVd4Qjs7QUFDQSxnQkFBSTVCLElBQUksQ0FBQ0ssR0FBTCxDQUFTRCxDQUFULEVBQVllLEVBQVosSUFBa0IsSUFBbEIsSUFBMEJuQixJQUFJLENBQUNLLEdBQUwsQ0FBU0QsQ0FBVCxFQUFZZSxFQUFaLElBQWtCLElBQWhELEVBQXNEO0FBQ2xEbkIsY0FBQUEsSUFBSSxDQUFDNkIsSUFBTCxDQUFVLENBQVYsSUFBZTdCLElBQUksQ0FBQzZCLElBQUwsQ0FBVSxDQUFWLElBQWUsQ0FBZixHQUFtQjdCLElBQUksQ0FBQ0ssR0FBTCxDQUFTRCxDQUFULEVBQVl3QixHQUFaLEdBQWtCLENBQXBEO0FBQ0ExQyxjQUFBQSxFQUFFLENBQUNLLElBQUgsQ0FBUSx5QkFBUixFQUFtQ0MsWUFBbkMsQ0FBZ0ROLEVBQUUsQ0FBQ1UsS0FBbkQsRUFBMERDLE1BQTFELEdBQW1FRyxJQUFJLENBQUM2QixJQUFMLENBQVUsQ0FBVixJQUFlLEdBQWYsR0FBcUI3QixJQUFJLENBQUM2QixJQUFMLENBQVUsQ0FBVixDQUF4RjtBQUNILGFBSEQsQ0FJQTtBQUpBLGlCQUtLLElBQUk3QixJQUFJLENBQUNLLEdBQUwsQ0FBU0QsQ0FBVCxFQUFZZSxFQUFaLElBQWtCLElBQXRCLEVBQTRCO0FBQzdCbkIsZ0JBQUFBLElBQUksQ0FBQzhCLElBQUwsQ0FBVSxDQUFWLElBQWU5QixJQUFJLENBQUM4QixJQUFMLENBQVUsQ0FBVixJQUFlLENBQWYsR0FBbUI5QixJQUFJLENBQUNLLEdBQUwsQ0FBU0QsQ0FBVCxFQUFZd0IsR0FBWixHQUFrQixDQUFwRDs7QUFDQSxvQkFBSTVCLElBQUksQ0FBQzhCLElBQUwsQ0FBVSxDQUFWLEtBQWdCOUIsSUFBSSxDQUFDOEIsSUFBTCxDQUFVLENBQVYsQ0FBcEIsRUFBa0M7QUFDOUI5QixrQkFBQUEsSUFBSSxDQUFDOEIsSUFBTCxDQUFVLENBQVYsSUFBZTlCLElBQUksQ0FBQzhCLElBQUwsQ0FBVSxDQUFWLENBQWY7QUFDSDs7QUFDRDVDLGdCQUFBQSxFQUFFLENBQUNLLElBQUgsQ0FBUSx5QkFBUixFQUFtQ0MsWUFBbkMsQ0FBZ0ROLEVBQUUsQ0FBQ1UsS0FBbkQsRUFBMERDLE1BQTFELEdBQW1FRyxJQUFJLENBQUM4QixJQUFMLENBQVUsQ0FBVixJQUFlLEdBQWYsR0FBcUI5QixJQUFJLENBQUM4QixJQUFMLENBQVUsQ0FBVixDQUF4RjtBQUNILGVBTkksQ0FPTDtBQVBLLG1CQVFBLElBQUk5QixJQUFJLENBQUNLLEdBQUwsQ0FBU0QsQ0FBVCxFQUFZZSxFQUFaLElBQWtCLElBQXRCLEVBQTRCO0FBQzdCbkIsa0JBQUFBLElBQUksQ0FBQytCLEtBQUwsQ0FBVyxDQUFYLElBQWdCL0IsSUFBSSxDQUFDK0IsS0FBTCxDQUFXLENBQVgsSUFBZ0IsQ0FBaEIsR0FBb0IvQixJQUFJLENBQUNLLEdBQUwsQ0FBU0QsQ0FBVCxFQUFZd0IsR0FBWixHQUFrQixDQUF0RDs7QUFDQSxzQkFBSTVCLElBQUksQ0FBQytCLEtBQUwsQ0FBVyxDQUFYLEtBQWlCL0IsSUFBSSxDQUFDK0IsS0FBTCxDQUFXLENBQVgsQ0FBckIsRUFBb0M7QUFDaEMvQixvQkFBQUEsSUFBSSxDQUFDK0IsS0FBTCxDQUFXLENBQVgsSUFBZ0IvQixJQUFJLENBQUMrQixLQUFMLENBQVcsQ0FBWCxDQUFoQjtBQUNIOztBQUNEN0Msa0JBQUFBLEVBQUUsQ0FBQ0ssSUFBSCxDQUFRLHlCQUFSLEVBQW1DQyxZQUFuQyxDQUFnRE4sRUFBRSxDQUFDVSxLQUFuRCxFQUEwREMsTUFBMUQsR0FBbUVHLElBQUksQ0FBQytCLEtBQUwsQ0FBVyxDQUFYLElBQWdCLEdBQWhCLEdBQXNCL0IsSUFBSSxDQUFDK0IsS0FBTCxDQUFXLENBQVgsQ0FBekY7QUFDSCxpQkEvQnVCLENBZ0N4Qjs7O0FBQ0F4QixZQUFBQSxJQUFJLENBQUNJLGNBQUwsQ0FBb0IsS0FBcEIsRUFBMkJuQixZQUEzQixDQUF3Q04sRUFBRSxDQUFDVSxLQUEzQyxFQUFrREMsTUFBbEQsR0FBMkRHLElBQUksQ0FBQ0ssR0FBTCxDQUFTRCxDQUFULEVBQVk0QixLQUF2RSxDQWpDd0IsQ0FrQ3hCOztBQUNBekIsWUFBQUEsSUFBSSxDQUFDZSxNQUFMLEdBQWNwQyxFQUFFLENBQUNLLElBQUgsQ0FBUSw2QkFBUixDQUFkO0FBQ0g7QUFDSixTQXhDWSxDQXlDYjs7O0FBQ0FTLFFBQUFBLElBQUksQ0FBQzZCLElBQUwsQ0FBVSxDQUFWLElBQWUsQ0FBQzdCLElBQUksQ0FBQzZCLElBQUwsQ0FBVSxDQUFWLElBQWUsQ0FBZixHQUFtQkksR0FBRyxDQUFDQyxHQUFKLEdBQVUsQ0FBOUIsRUFBaUNDLE9BQWpDLENBQXlDLENBQXpDLENBQWY7O0FBQ0EsWUFBSW5DLElBQUksQ0FBQzZCLElBQUwsQ0FBVSxDQUFWLEtBQWdCLENBQXBCLEVBQXVCO0FBQ25CTyxVQUFBQSxPQUFPLENBQUNDLEdBQVIsQ0FBWSxNQUFaO0FBQ0g7O0FBQ0QsWUFBSXJDLElBQUksQ0FBQzZCLElBQUwsQ0FBVSxDQUFWLEtBQWdCN0IsSUFBSSxDQUFDNkIsSUFBTCxDQUFVLENBQVYsQ0FBcEIsRUFBa0M7QUFDOUI3QixVQUFBQSxJQUFJLENBQUM2QixJQUFMLENBQVUsQ0FBVixJQUFlL0IsSUFBSSxDQUFDQyxLQUFMLENBQVdDLElBQUksQ0FBQzZCLElBQUwsQ0FBVSxDQUFWLENBQVgsQ0FBZjtBQUNILFNBaERZLENBa0RiOzs7QUFDQTNDLFFBQUFBLEVBQUUsQ0FBQ0ssSUFBSCxDQUFRLHlCQUFSLEVBQW1DQyxZQUFuQyxDQUFnRE4sRUFBRSxDQUFDVSxLQUFuRCxFQUEwREMsTUFBMUQsR0FBbUVHLElBQUksQ0FBQzZCLElBQUwsQ0FBVSxDQUFWLElBQWUsR0FBZixHQUFxQjdCLElBQUksQ0FBQzZCLElBQUwsQ0FBVSxDQUFWLENBQXhGLENBbkRhLENBcURiOztBQUNBM0MsUUFBQUEsRUFBRSxDQUFDSyxJQUFILENBQVEsUUFBUixFQUFrQitDLGlCQUFsQjtBQUNBcEQsUUFBQUEsRUFBRSxDQUFDSyxJQUFILENBQVEsYUFBUixFQUF1QlcsTUFBdkIsR0FBZ0MsSUFBaEM7QUFDQWhCLFFBQUFBLEVBQUUsQ0FBQ0ssSUFBSCxDQUFRLGFBQVIsRUFBdUJXLE1BQXZCLEdBQWdDLElBQWhDLENBeERhLENBMERiOztBQUNBaEIsUUFBQUEsRUFBRSxDQUFDSyxJQUFILENBQVEsZ0JBQVIsRUFBMEJDLFlBQTFCLENBQXVDTixFQUFFLENBQUNxQyxNQUExQyxFQUFrRGdCLFlBQWxELEdBQWlFLElBQWpFO0FBQ0FyRCxRQUFBQSxFQUFFLENBQUNLLElBQUgsQ0FBUSxnQkFBUixFQUEwQkMsWUFBMUIsQ0FBdUNOLEVBQUUsQ0FBQ3FDLE1BQTFDLEVBQWtEQyxXQUFsRCxDQUE4RCxDQUE5RCxFQUFpRUMsZUFBakUsR0FBbUYsSUFBbkY7QUFDQXZDLFFBQUFBLEVBQUUsQ0FBQ0ssSUFBSCxDQUFRLGdCQUFSLEVBQTBCQyxZQUExQixDQUF1Q04sRUFBRSxDQUFDMEIsTUFBMUMsRUFBa0RDLFdBQWxELEdBQWdFM0IsRUFBRSxDQUFDSyxJQUFILENBQVEsTUFBUixFQUFnQkMsWUFBaEIsQ0FBNkIsTUFBN0IsRUFBcUNnRCxJQUFyQyxDQUEwQ3pCLGNBQTFDLENBQXlELElBQXpELENBQWhFO0FBRUE3QixRQUFBQSxFQUFFLENBQUNLLElBQUgsQ0FBUSx5QkFBUixFQUFtQ1csTUFBbkMsR0FBNEMsS0FBNUM7QUFDQUYsUUFBQUEsSUFBSSxDQUFDeUMsRUFBTCxHQUFVLENBQUMsQ0FBWDtBQUNBdkQsUUFBQUEsRUFBRSxDQUFDSyxJQUFILENBQVEsYUFBUixFQUF1QkMsWUFBdkIsQ0FBb0NOLEVBQUUsQ0FBQ08sU0FBdkMsRUFBa0RDLElBQWxEO0FBQ0FSLFFBQUFBLEVBQUUsQ0FBQ0ssSUFBSCxDQUFRLFFBQVIsRUFBa0JtRCxrQkFBbEI7QUFDSCxPQW5FUyxFQW1FUCxJQW5FTyxDQUFWO0FBb0VILEtBdkdTLEVBdUdQLElBdkdPLENBQVY7QUF3R0g7QUFqSEksQ0FBVCIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8gTGVhcm4gY2MuQ2xhc3M6XHJcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2NsYXNzLmh0bWxcclxuLy8gTGVhcm4gQXR0cmlidXRlOlxyXG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXHJcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxyXG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXHJcblxyXG5jYy5DbGFzcyh7XHJcbiAgICBleHRlbmRzOiBjYy5Db21wb25lbnQsXHJcblxyXG4gICAgcHJvcGVydGllczoge1xyXG5cclxuICAgIH0sXHJcblxyXG4gICAg5pel56iL5pu05pawKCkge1xyXG4gICAgICAgIGNjLmZpbmQoJ0NhbnZhcy/pu5HlsY8nKS5nZXRDb21wb25lbnQoY2MuQW5pbWF0aW9uKS5wbGF5KCfpu5HlsY8nKTtcclxuICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICAgICAgY2MuZmluZCgnQ2FudmFzL+m7keWxjy/ml7bpl7QnKS5nZXRDb21wb25lbnQoY2MuTGFiZWwpLnN0cmluZyA9IE1hdGguZmxvb3Ioc2VsZi7ml7bpl7QgLyA0KSArICflubQnICsgc2VsZi7ml7bpl7QgJSA0ICsgJ+aciCc7XHJcbiAgICAgICAgICAgIGNjLmZpbmQoJ0NhbnZhcy/pu5HlsY8v5pe26Ze0JykuYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICAgICAgY2MuZmluZCgnQ2FudmFzL+WIneWni+eVjOmdoicpLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICAvL+S6uueJqee7hOWkhOeQhlxyXG4gICAgICAgICAgICAvL+a4heepuuWOn+acieS6uueJqee7hFxyXG4gICAgICAgICAgICBjYy5maW5kKCdDYW52YXMv5LiL55WM6Z2iL+S6uueJqeaxoC92aWV3L2NvbnRlbnQnKS5kZXN0cm95QWxsQ2hpbGRyZW4oKTtcclxuICAgICAgICAgICAgY2MuZmluZCgnQ2FudmFzL+S4reW3pueVjOmdoi92aWV3L2NvbnRlbnQnKS5kZXN0cm95QWxsQ2hpbGRyZW4oKTtcclxuXHJcbiAgICAgICAgICAgIC8v55Sf5oiQ5paw5Lq654mp57uEXHJcbiAgICAgICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgc2VsZi7kurrniannu4QubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICAgIHZhciBub2RlID0gY2MuaW5zdGFudGlhdGUoY2MuZmluZCgnZ2FtZScpLmdldENvbXBvbmVudCgnZ2FtZScpLuS6uueJqSk7XHJcbiAgICAgICAgICAgICAgICBpZiAoc2VsZi7kurrniannu4RbaV0u5oCn5YirID09ICfnlLcnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgbm9kZS5nZXRDaGlsZEJ5TmFtZSgn5aS05YOPJykuZ2V0Q29tcG9uZW50KGNjLlNwcml0ZSkuc3ByaXRlRnJhbWUgPSBjYy5maW5kKCdnYW1lJykuZ2V0Q29tcG9uZW50KCdnYW1lJyku55S3LmdldFNwcml0ZUZyYW1lKHNlbGYu5Lq654mp57uEW2ldLuWktOWDjyk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIG5vZGUuZ2V0Q2hpbGRCeU5hbWUoJ+WktOWDjycpLmdldENvbXBvbmVudChjYy5TcHJpdGUpLnNwcml0ZUZyYW1lID0gY2MuZmluZCgnZ2FtZScpLmdldENvbXBvbmVudCgnZ2FtZScpLuWlsy5nZXRTcHJpdGVGcmFtZShzZWxmLuS6uueJqee7hFtpXS7lpLTlg48pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgbm9kZS5nZXRDaGlsZEJ5TmFtZSgn5aS05YOPJykuZ2V0Q2hpbGRCeU5hbWUoJ+WQjeWtlycpLmdldENvbXBvbmVudChjYy5MYWJlbCkuc3RyaW5nID0gc2VsZi7kurrniannu4RbaV0u5ZCN5a2XO1xyXG4gICAgICAgICAgICAgICAgbm9kZS5nZXRDaGlsZEJ5TmFtZSgn5bel5L2cJykuZ2V0Q29tcG9uZW50KGNjLkxhYmVsKS5zdHJpbmcgPSBzZWxmLuS6uueJqee7hFtpXS7lt6XkvZw7XHJcbiAgICAgICAgICAgICAgICBub2RlLmdldENoaWxkQnlOYW1lKCfmjqfliLbmnYMnKS5nZXRDb21wb25lbnQoY2MuTGFiZWwpLnN0cmluZyA9IHNlbGYu5Lq654mp57uEW2ldLuaOp+WItuadgztcclxuICAgICAgICAgICAgICAgIG5vZGUubmFtZSA9IGkgKyAnJztcclxuICAgICAgICAgICAgICAgIGlmIChzZWxmLuS6uueJqee7hFtpXS7mjqfliLbmnYMgPT0gc2VsZi5uYW1lKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgbm9kZS5wYXJlbnQgPSBjYy5maW5kKCdDYW52YXMv5LiL55WM6Z2iL+S6uueJqeaxoC92aWV3L2NvbnRlbnQnKTtcclxuICAgICAgICAgICAgICAgICAgICBub2RlLmdldENvbXBvbmVudChjYy5CdXR0b24pLmNsaWNrRXZlbnRzWzBdLmN1c3RvbUV2ZW50RGF0YSA9ICfoh6rlt7EnO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBub2RlLnBhcmVudCA9IGNjLmZpbmQoJ0NhbnZhcy/kuK3lt6bnlYzpnaIvdmlldy9jb250ZW50Jyk7XHJcbiAgICAgICAgICAgICAgICAgICAgbm9kZS5nZXRDb21wb25lbnQoJ+S6uueJqeeugOS7iycpLuazqOWGjOeDremUrigpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgY2MuZmluZCgnQ2FudmFzL+S4i+eVjOmdoi/kurrniannirbmgIEv5L+h5oGvL3ZpZXcvY29udGVudCcpLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGNjLmZpbmQoJ0NhbnZhcy/pu5HlsY8v5pe26Ze0JykuYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgICAgIGNjLmZpbmQoJ0NhbnZhcy/pu5HlsY8nKS5nZXRDb21wb25lbnQoY2MuQW5pbWF0aW9uKS5wbGF5KCfnmb3lsY8nKTtcclxuICAgICAgICAgICAgY2MuZmluZCgnQ2FudmFzL+aXpeeoi+ivpuaDhS/lt6XkvZwvdmlldy9jb250ZW50JykuZGVzdHJveUFsbENoaWxkcmVuKCk7XHJcblxyXG4gICAgICAgICAgICAvLyDml6XnqIvor6bmg4Xmt7vliqDkurrnianlt6XkvZxcclxuICAgICAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHNlbGYu5Lq654mp57uELmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFyIG5vZGUgPSBjYy5pbnN0YW50aWF0ZShjYy5maW5kKCdnYW1lJykuZ2V0Q29tcG9uZW50KCdnYW1lJyku5pel56iL6K+m5oOFX+W3peS9nCk7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHNlbGYu5Lq654mp57uEW2ldLuW3peS9nCAhPSAn5LyR5oGvJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvL+WkjeWItuiKgueCuei/m+aXpeeoi+ivpuaDhVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoc2VsZi7kurrniannu4RbaV0u5oCn5YirID09ICfnlLcnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBub2RlLmdldENoaWxkQnlOYW1lKCflpLTlg48nKS5nZXRDb21wb25lbnQoY2MuU3ByaXRlKS5zcHJpdGVGcmFtZSA9IGNjLmZpbmQoJ2dhbWUnKS5nZXRDb21wb25lbnQoJ2dhbWUnKS7nlLcuZ2V0U3ByaXRlRnJhbWUoc2VsZi7kurrniannu4RbaV0u5aS05YOPKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5vZGUuZ2V0Q2hpbGRCeU5hbWUoJ+WktOWDjycpLmdldENvbXBvbmVudChjYy5TcHJpdGUpLnNwcml0ZUZyYW1lID0gY2MuZmluZCgnZ2FtZScpLmdldENvbXBvbmVudCgnZ2FtZScpLuWlsy5nZXRTcHJpdGVGcmFtZShzZWxmLuS6uueJqee7hFtpXS7lpLTlg48pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG5vZGUuZ2V0Q2hpbGRCeU5hbWUoJ+WQjeWtlycpLmdldENvbXBvbmVudChjYy5MYWJlbCkuc3RyaW5nID0gc2VsZi7kurrniannu4RbaV0u5ZCN5a2XICsgJygnICsgc2VsZi7kurrniannu4RbaV0u5o6n5Yi25p2DICsgJyknO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBub2RlLmdldENoaWxkQnlOYW1lKCflt6XkvZwnKS5nZXRDb21wb25lbnQoY2MuTGFiZWwpLnN0cmluZyA9IHNlbGYu5Lq654mp57uEW2ldLuW3peS9nDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbm9kZS5nZXRDaGlsZEJ5TmFtZSgn5pS26I636YePJykuZ2V0Q29tcG9uZW50KGNjLkxhYmVsKS5zdHJpbmcgPSBzZWxmLuS6uueJqee7hFtpXS7mlLbojrfph487XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvL+mjn+eJqemHh+mbhuiuoeeul1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoc2VsZi7kurrniannu4RbaV0u5bel5L2cID09ICfph4fpm4YnIHx8IHNlbGYu5Lq654mp57uEW2ldLuW3peS9nCA9PSAn54up54yOJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2VsZi5mb29kWzBdID0gc2VsZi5mb29kWzBdICogMSArIHNlbGYu5Lq654mp57uEW2ldLuaUtuiOt+mHjyAqIDE7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYy5maW5kKCdDYW52YXMv5LiK55WM6Z2iL+WMuuWfn+eVjOmdoi/otYTmupAv6aOf54mpLzInKS5nZXRDb21wb25lbnQoY2MuTGFiZWwpLnN0cmluZyA9IHNlbGYuZm9vZFswXSArICcvJyArIHNlbGYuZm9vZFsxXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvL+acqOWktOmHh+mbhuiuoeeul1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBlbHNlIGlmIChzZWxmLuS6uueJqee7hFtpXS7lt6XkvZwgPT0gJ+egjeS8kCcpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlbGYubW9vZFswXSA9IHNlbGYubW9vZFswXSAqIDEgKyBzZWxmLuS6uueJqee7hFtpXS7mlLbojrfph48gKiAxO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHNlbGYubW9vZFswXSA+PSBzZWxmLm1vb2RbMV0pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZWxmLm1vb2RbMF0gPSBzZWxmLm1vb2RbMV07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYy5maW5kKCdDYW52YXMv5LiK55WM6Z2iL+WMuuWfn+eVjOmdoi/otYTmupAv5pyo5aS0LzInKS5nZXRDb21wb25lbnQoY2MuTGFiZWwpLnN0cmluZyA9IHNlbGYubW9vZFswXSArICcvJyArIHNlbGYubW9vZFsxXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvL+efs+WktOmHh+mbhuiuoeeul1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBlbHNlIGlmIChzZWxmLuS6uueJqee7hFtpXS7lt6XkvZwgPT0gJ+aMluaOmCcpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlbGYuc3RvbmVbMF0gPSBzZWxmLnN0b25lWzBdICogMSArIHNlbGYu5Lq654mp57uEW2ldLuaUtuiOt+mHjyAqIDE7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoc2VsZi5zdG9uZVswXSA+PSBzZWxmLnN0b25lWzFdKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2VsZi5zdG9uZVswXSA9IHNlbGYuc3RvbmVbMV07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYy5maW5kKCdDYW52YXMv5LiK55WM6Z2iL+WMuuWfn+eVjOmdoi/otYTmupAv55+z5aS0LzInKS5nZXRDb21wb25lbnQoY2MuTGFiZWwpLnN0cmluZyA9IHNlbGYuc3RvbmVbMF0gKyAnLycgKyBzZWxmLnN0b25lWzFdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8v57uP6aqM5YC86K6h566XXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG5vZGUuZ2V0Q2hpbGRCeU5hbWUoJ+e7j+mqjOWAvCcpLmdldENvbXBvbmVudChjYy5MYWJlbCkuc3RyaW5nID0gc2VsZi7kurrniannu4RbaV0u5pS26I6357uP6aqM5YC8O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvL+e7keWumueItuiKgueCuVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBub2RlLnBhcmVudCA9IGNjLmZpbmQoJ0NhbnZhcy/ml6XnqIvor6bmg4Uv5bel5L2cL3ZpZXcvY29udGVudCcpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIC8v6aOf54mp5raI6ICX6K6h566XXHJcbiAgICAgICAgICAgICAgICBzZWxmLmZvb2RbMF0gPSAoc2VsZi5mb29kWzBdICogMSAtIG1zZy7ov5vpo5/ph48gKiAxKS50b0ZpeGVkKDIpO1xyXG4gICAgICAgICAgICAgICAgaWYgKHNlbGYuZm9vZFswXSA8PSAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ+a4uOaIj+e7k+adnycpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgaWYgKHNlbGYuZm9vZFswXSA+PSBzZWxmLmZvb2RbMV0pIHtcclxuICAgICAgICAgICAgICAgICAgICBzZWxmLmZvb2RbMF0gPSBNYXRoLmZsb29yKHNlbGYuZm9vZFsxXSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgLy/po5/nianmm7TmlrBcclxuICAgICAgICAgICAgICAgIGNjLmZpbmQoJ0NhbnZhcy/kuIrnlYzpnaIv5Yy65Z+f55WM6Z2iL+i1hOa6kC/po5/niakvMicpLmdldENvbXBvbmVudChjYy5MYWJlbCkuc3RyaW5nID0gc2VsZi5mb29kWzBdICsgJy8nICsgc2VsZi5mb29kWzFdO1xyXG5cclxuICAgICAgICAgICAgICAgIC8v5omT5byA5pel56iL6K+m5oOF55WM6Z2iXHJcbiAgICAgICAgICAgICAgICBjYy5maW5kKCdDYW52YXMnKS5wYXVzZVN5c3RlbUV2ZW50cygpO1xyXG4gICAgICAgICAgICAgICAgY2MuZmluZCgnQ2FudmFzL+S6jOe6p+mBrue9qScpLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICBjYy5maW5kKCdDYW52YXMv5pel56iL6K+m5oOFJykuYWN0aXZlID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgICAgICAgICAvL+WHhuWkh+eKtuaAgemHjee9rlxyXG4gICAgICAgICAgICAgICAgY2MuZmluZCgnQ2FudmFzL+S4reWPs+eVjOmdoi/lh4blpIcnKS5nZXRDb21wb25lbnQoY2MuQnV0dG9uKS5pbnRlcmFjdGFibGUgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgY2MuZmluZCgnQ2FudmFzL+S4reWPs+eVjOmdoi/lh4blpIcnKS5nZXRDb21wb25lbnQoY2MuQnV0dG9uKS5jbGlja0V2ZW50c1sxXS5jdXN0b21FdmVudERhdGEgPSAn5YeG5aSHJztcclxuICAgICAgICAgICAgICAgIGNjLmZpbmQoJ0NhbnZhcy/kuK3lj7PnlYzpnaIv5YeG5aSHJykuZ2V0Q29tcG9uZW50KGNjLlNwcml0ZSkuc3ByaXRlRnJhbWUgPSBjYy5maW5kKCdnYW1lJykuZ2V0Q29tcG9uZW50KCdnYW1lJyku5YeG5aSH54q25oCBLmdldFNwcml0ZUZyYW1lKCflh4blpIcnKTtcclxuXHJcbiAgICAgICAgICAgICAgICBjYy5maW5kKCdDYW52YXMv5LiL55WM6Z2iL+S6uueJqeaxoC92aWV3L+mAieS4reahhicpLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgc2VsZi7pgInkuK0gPSAtMTtcclxuICAgICAgICAgICAgICAgIGNjLmZpbmQoJ0NhbnZhcy/ml6XnqIvor6bmg4UnKS5nZXRDb21wb25lbnQoY2MuQW5pbWF0aW9uKS5wbGF5KCk7XHJcbiAgICAgICAgICAgICAgICBjYy5maW5kKCdDYW52YXMnKS5yZXN1bWVTeXN0ZW1FdmVudHMoKTtcclxuICAgICAgICAgICAgfSwgMzAwMCk7XHJcbiAgICAgICAgfSwgMzAwMCk7XHJcbiAgICB9XHJcbn0pO1xyXG4iXX0=