
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/JavaScript/game/聊天换行.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '8641dCtjOJJJIQdfvihStOi', '聊天换行');
// JavaScript/game/聊天换行.js

"use strict";

// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
cc.Class({
  "extends": cc.Component,
  properties: {
    node1: cc.Node
  },
  onLoad: function onLoad() {
    var len = this.node.getComponent(cc.Label).string.length;

    if (len <= 13) {
      this.node.width = len * 42;
    } else {
      this.node.width = 528;
    }
  }
});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcSmF2YVNjcmlwdFxcZ2FtZVxc6IGK5aSp5o2i6KGMLmpzIl0sIm5hbWVzIjpbImNjIiwiQ2xhc3MiLCJDb21wb25lbnQiLCJwcm9wZXJ0aWVzIiwibm9kZTEiLCJOb2RlIiwib25Mb2FkIiwibGVuIiwibm9kZSIsImdldENvbXBvbmVudCIsIkxhYmVsIiwic3RyaW5nIiwibGVuZ3RoIiwid2lkdGgiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUFBLEVBQUUsQ0FBQ0MsS0FBSCxDQUFTO0FBQ0wsYUFBU0QsRUFBRSxDQUFDRSxTQURQO0FBR0xDLEVBQUFBLFVBQVUsRUFBRTtBQUNSQyxJQUFBQSxLQUFLLEVBQUVKLEVBQUUsQ0FBQ0s7QUFERixHQUhQO0FBTUxDLEVBQUFBLE1BTkssb0JBTUk7QUFDTCxRQUFJQyxHQUFHLEdBQUcsS0FBS0MsSUFBTCxDQUFVQyxZQUFWLENBQXVCVCxFQUFFLENBQUNVLEtBQTFCLEVBQWlDQyxNQUFqQyxDQUF3Q0MsTUFBbEQ7O0FBQ0EsUUFBSUwsR0FBRyxJQUFJLEVBQVgsRUFBZTtBQUNYLFdBQUtDLElBQUwsQ0FBVUssS0FBVixHQUFrQk4sR0FBRyxHQUFHLEVBQXhCO0FBQ0gsS0FGRCxNQUVPO0FBQ0gsV0FBS0MsSUFBTCxDQUFVSyxLQUFWLEdBQWtCLEdBQWxCO0FBQ0g7QUFDSjtBQWJJLENBQVQiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8vIExlYXJuIGNjLkNsYXNzOlxyXG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9jbGFzcy5odG1sXHJcbi8vIExlYXJuIEF0dHJpYnV0ZTpcclxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxyXG4vLyBMZWFybiBsaWZlLWN5Y2xlIGNhbGxiYWNrczpcclxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvbGlmZS1jeWNsZS1jYWxsYmFja3MuaHRtbFxyXG5cclxuY2MuQ2xhc3Moe1xyXG4gICAgZXh0ZW5kczogY2MuQ29tcG9uZW50LFxyXG5cclxuICAgIHByb3BlcnRpZXM6IHtcclxuICAgICAgICBub2RlMTogY2MuTm9kZSxcclxuICAgIH0sXHJcbiAgICBvbkxvYWQoKSB7XHJcbiAgICAgICAgdmFyIGxlbiA9IHRoaXMubm9kZS5nZXRDb21wb25lbnQoY2MuTGFiZWwpLnN0cmluZy5sZW5ndGg7XHJcbiAgICAgICAgaWYgKGxlbiA8PSAxMykge1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUud2lkdGggPSBsZW4gKiA0MjtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUud2lkdGggPSA1Mjg7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcblxyXG5cclxufSk7XHJcbiJdfQ==